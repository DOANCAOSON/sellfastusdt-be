/*
  Warnings:

  - You are about to drop the column `price` on the `ethereum_wallet` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "ethereum_wallet" DROP COLUMN "price";
