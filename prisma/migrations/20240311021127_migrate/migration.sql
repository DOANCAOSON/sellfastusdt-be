-- CreateTable
CREATE TABLE "otps" (
    "id" VARCHAR(16) NOT NULL,
    "otp" TEXT NOT NULL,
    "userId" VARCHAR(16) NOT NULL,
    "expiresAt" DOUBLE PRECISION NOT NULL,
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "otps_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "network" (
    "id" VARCHAR(16) NOT NULL,
    "name" TEXT NOT NULL,
    "slug" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),
    "label" TEXT,
    "description" TEXT,
    "value" TEXT,
    "isActive" BOOLEAN NOT NULL DEFAULT true,

    CONSTRAINT "network_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "wallets" (
    "id" VARCHAR(16) NOT NULL,
    "userId" VARCHAR(16) NOT NULL,
    "binance" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),
    "tokenId" VARCHAR(16) NOT NULL,
    "amount" DOUBLE PRECISION NOT NULL DEFAULT 0,
    "address" TEXT,
    "isActive" BOOLEAN NOT NULL DEFAULT true,

    CONSTRAINT "wallets_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "address_wallets" (
    "id" VARCHAR(16) NOT NULL,
    "networkId" VARCHAR(16) NOT NULL,
    "userId" VARCHAR(16) NOT NULL,
    "addressWallet" TEXT NOT NULL DEFAULT '',
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),

    CONSTRAINT "address_wallets_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "users" (
    "id" VARCHAR(16) NOT NULL,
    "email" TEXT,
    "resetPasswordToken" TEXT,
    "reset_password_token" TEXT[],
    "confirmation_token" TEXT,
    "blocked" BOOLEAN,
    "blocked_at" TIMESTAMP(3),
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),

    CONSTRAINT "users_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "owner_users" (
    "id" VARCHAR(16) NOT NULL,
    "email" TEXT,
    "isActive" BOOLEAN,
    "addresses" TEXT[],
    "blocked_at" TIMESTAMP(3),
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),

    CONSTRAINT "owner_users_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "admin_users" (
    "id" VARCHAR(16) NOT NULL,
    "firstname" VARCHAR(50),
    "lastname" VARCHAR(50),
    "username" VARCHAR(50),
    "email" VARCHAR(50),
    "password" VARCHAR(100),
    "reset_password_token" TEXT,
    "registration_token" TEXT,
    "is_active" BOOLEAN DEFAULT false,
    "blocked" BOOLEAN DEFAULT false,
    "blocked_at" TIMESTAMP(3),
    "preferred_language" VARCHAR(10),
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),
    "created_by_id" VARCHAR(16),
    "updated_by_id" VARCHAR(16),

    CONSTRAINT "admin_users_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "article" (
    "id" VARCHAR(16) NOT NULL,
    "title" TEXT NOT NULL,
    "slug" TEXT NOT NULL,
    "description" TEXT,
    "body" TEXT NOT NULL,
    "images" TEXT,
    "images_upload" JSONB[],
    "published" BOOLEAN NOT NULL DEFAULT false,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "created_by_id" VARCHAR(16),
    "updated_by_id" VARCHAR(16),
    "adminUserId" VARCHAR(16),

    CONSTRAINT "article_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "balance" (
    "id" VARCHAR(16) NOT NULL,
    "ownerId" VARCHAR(16),
    "userId" VARCHAR(16) NOT NULL,
    "code" TEXT NOT NULL DEFAULT '',
    "amount" DOUBLE PRECISION NOT NULL DEFAULT 0,
    "type" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),
    "status" TEXT NOT NULL,
    "isActive" BOOLEAN NOT NULL DEFAULT true,
    "created_by_id" VARCHAR(16),
    "updated_by_id" VARCHAR(16),
    "netWorkId" TEXT,
    "symbolId" TEXT NOT NULL,
    "addressClientId" VARCHAR(16),
    "addressOwnerId" VARCHAR(16),
    "buyed_at" TIMESTAMP(3),
    "selled_at" TIMESTAMP(3),
    "confirmed_at" TIMESTAMP(3),
    "cancel_at" TIMESTAMP(3),

    CONSTRAINT "balance_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "balance_history" (
    "id" VARCHAR(16) NOT NULL,
    "userId" VARCHAR(16) NOT NULL,
    "type" VARCHAR(16) NOT NULL,
    "amount" DOUBLE PRECISION NOT NULL DEFAULT 0,
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),
    "created_by_id" VARCHAR(16),
    "updated_by_id" VARCHAR(16),
    "action" JSONB[],
    "ownerUsersId" VARCHAR(16),
    "buyed_at" TIMESTAMP(3),
    "selled_at" TIMESTAMP(3),
    "confirmed_at" TIMESTAMP(3),
    "cancel_at" TIMESTAMP(3),
    "balanceId" VARCHAR(16),

    CONSTRAINT "balance_history_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "notifications" (
    "id" VARCHAR(16) NOT NULL,
    "userId" VARCHAR(16) NOT NULL,
    "url" TEXT,
    "text" TEXT,
    "content" TEXT,
    "image" TEXT,
    "isRead" BOOLEAN NOT NULL DEFAULT false,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "notifications_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "ethereum_wallet" (
    "id" VARCHAR(16) NOT NULL,
    "tokenName" VARCHAR(32) NOT NULL,
    "price" DOUBLE PRECISION NOT NULL DEFAULT 0,
    "rateBuy" DOUBLE PRECISION NOT NULL DEFAULT 0,
    "rateSell" DOUBLE PRECISION NOT NULL DEFAULT 0,
    "binance" TEXT NOT NULL,
    "fee" DOUBLE PRECISION NOT NULL DEFAULT 0,
    "images" TEXT,
    "images_upload" JSONB[],
    "slug" TEXT NOT NULL,
    "isActive" BOOLEAN NOT NULL,
    "whitepaper" TEXT NOT NULL,
    "max" DOUBLE PRECISION NOT NULL,
    "min" DOUBLE PRECISION NOT NULL,
    "networkIds" TEXT[],
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),

    CONSTRAINT "ethereum_wallet_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "transaction" (
    "id" VARCHAR(16) NOT NULL,
    "code" TEXT NOT NULL,
    "ip" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "ethereumAmount" DOUBLE PRECISION NOT NULL DEFAULT 0,
    "coinAmount" DOUBLE PRECISION NOT NULL DEFAULT 0,
    "status" TEXT NOT NULL,
    "binance" TEXT,
    "userId" VARCHAR(16) NOT NULL,
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),
    "expired" DOUBLE PRECISION,
    "transactionToken" TEXT,
    "buyed_at" TIMESTAMP(3),
    "selled_at" TIMESTAMP(3),
    "confirmed_at" TIMESTAMP(3),
    "cancel_at" TIMESTAMP(3),
    "ownerUsersId" VARCHAR(16),

    CONSTRAINT "transaction_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "transaction_history" (
    "id" VARCHAR(16) NOT NULL,
    "userId" VARCHAR(16) NOT NULL,
    "transactionId" VARCHAR(16) NOT NULL,
    "type" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),
    "created_by_id" VARCHAR(16),
    "updated_by_id" VARCHAR(16),
    "action" JSONB[],
    "ownerUsersId" VARCHAR(16),

    CONSTRAINT "transaction_history_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "admin_roles" (
    "id" VARCHAR(16) NOT NULL,
    "name" VARCHAR(50) NOT NULL,
    "code" TEXT,
    "description" VARCHAR(100),
    "permissions" JSONB[],
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),
    "created_by_id" VARCHAR(16),
    "updated_by_id" VARCHAR(16),

    CONSTRAINT "admin_roles_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "admin_users_roles_links" (
    "id" VARCHAR(16) NOT NULL,
    "user_id" VARCHAR(16),
    "role_id" VARCHAR(16),

    CONSTRAINT "admin_users_roles_links_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "api_token_permissions" (
    "id" VARCHAR(16) NOT NULL,
    "action" TEXT,
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),
    "created_by_id" VARCHAR(16),
    "updated_by_id" VARCHAR(16),

    CONSTRAINT "api_token_permissions_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "api_token_permissions_token_links" (
    "id" VARCHAR(16) NOT NULL,
    "api_token_permission_id" VARCHAR(16),
    "api_token_id" VARCHAR(16),

    CONSTRAINT "api_token_permissions_token_links_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "api_tokens" (
    "id" VARCHAR(16) NOT NULL,
    "name" TEXT,
    "description" TEXT,
    "type" TEXT,
    "access_key" TEXT,
    "last_used_at" TIMESTAMP(3),
    "expires_at" TIMESTAMP(3),
    "lifespan" BIGINT,
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),
    "created_by_id" VARCHAR(16),
    "updated_by_id" VARCHAR(16),

    CONSTRAINT "api_tokens_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "transfer_token_permissions" (
    "id" VARCHAR(16) NOT NULL,
    "action" TEXT,
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),
    "created_by_id" VARCHAR(16),
    "updated_by_id" VARCHAR(16),

    CONSTRAINT "transfer_token_permissions_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "transfer_token_permissions_token_links" (
    "id" VARCHAR(16) NOT NULL,
    "transfer_token_permission_id" VARCHAR(16),
    "transfer_token_id" VARCHAR(16),

    CONSTRAINT "transfer_token_permissions_token_links_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "transfer_tokens" (
    "id" VARCHAR(16) NOT NULL,
    "name" TEXT,
    "description" TEXT,
    "access_key" TEXT,
    "last_used_at" TIMESTAMP(3),
    "expires_at" TIMESTAMP(3),
    "lifespan" BIGINT,
    "created_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3),
    "created_by_id" VARCHAR(16),
    "updated_by_id" VARCHAR(16),

    CONSTRAINT "transfer_tokens_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "static_pages" (
    "id" VARCHAR(16) NOT NULL,
    "slug" TEXT NOT NULL,
    "html" TEXT,
    "created_at" TIMESTAMP(3),
    "updated_at" TIMESTAMP(3),
    "created_by_id" VARCHAR(16),
    "updated_by_id" VARCHAR(16),

    CONSTRAINT "static_pages_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "network_name_key" ON "network"("name");

-- CreateIndex
CREATE UNIQUE INDEX "network_slug_key" ON "network"("slug");

-- CreateIndex
CREATE INDEX "address_wallets_userId_idx" ON "address_wallets"("userId");

-- CreateIndex
CREATE UNIQUE INDEX "users_email_key" ON "users"("email");

-- CreateIndex
CREATE UNIQUE INDEX "owner_users_email_key" ON "owner_users"("email");

-- CreateIndex
CREATE UNIQUE INDEX "admin_users_email_key" ON "admin_users"("email");

-- CreateIndex
CREATE INDEX "admin_users_created_at_idx" ON "admin_users"("created_at");

-- CreateIndex
CREATE INDEX "admin_users_updated_at_idx" ON "admin_users"("updated_at");

-- CreateIndex
CREATE INDEX "admin_users_updated_by_id_idx" ON "admin_users"("updated_by_id");

-- CreateIndex
CREATE INDEX "admin_users_created_by_id_idx" ON "admin_users"("created_by_id");

-- CreateIndex
CREATE UNIQUE INDEX "article_title_key" ON "article"("title");

-- CreateIndex
CREATE UNIQUE INDEX "article_slug_key" ON "article"("slug");

-- CreateIndex
CREATE INDEX "article_createdAt_idx" ON "article"("createdAt");

-- CreateIndex
CREATE INDEX "article_updatedAt_idx" ON "article"("updatedAt");

-- CreateIndex
CREATE INDEX "articles_updated_by_id_fk" ON "article"("updated_by_id");

-- CreateIndex
CREATE INDEX "articles_created_by_id_fk" ON "article"("created_by_id");

-- CreateIndex
CREATE INDEX "balance_created_at_idx" ON "balance"("created_at");

-- CreateIndex
CREATE INDEX "balance_updated_at_idx" ON "balance"("updated_at");

-- CreateIndex
CREATE INDEX "balance_updated_by_id_idx" ON "balance"("updated_by_id");

-- CreateIndex
CREATE INDEX "balance_created_by_id_idx" ON "balance"("created_by_id");

-- CreateIndex
CREATE INDEX "balance_history_created_at_idx" ON "balance_history"("created_at");

-- CreateIndex
CREATE INDEX "balance_history_updated_at_idx" ON "balance_history"("updated_at");

-- CreateIndex
CREATE INDEX "balance_history_updated_by_id_idx" ON "balance_history"("updated_by_id");

-- CreateIndex
CREATE INDEX "balance_history_created_by_id_idx" ON "balance_history"("created_by_id");

-- CreateIndex
CREATE INDEX "ethereum_wallet_created_at_idx" ON "ethereum_wallet"("created_at");

-- CreateIndex
CREATE INDEX "ethereum_wallet_updated_at_idx" ON "ethereum_wallet"("updated_at");

-- CreateIndex
CREATE INDEX "transaction_created_at_idx" ON "transaction"("created_at");

-- CreateIndex
CREATE INDEX "transaction_userId_idx" ON "transaction"("userId");

-- CreateIndex
CREATE INDEX "transaction_ownerUsersId_idx" ON "transaction"("ownerUsersId");

-- CreateIndex
CREATE INDEX "transaction_updated_at_idx" ON "transaction"("updated_at");

-- CreateIndex
CREATE INDEX "transaction_history_created_at_idx" ON "transaction_history"("created_at");

-- CreateIndex
CREATE INDEX "transaction_history_updated_at_idx" ON "transaction_history"("updated_at");

-- CreateIndex
CREATE INDEX "transaction_history_updated_by_id_idx" ON "transaction_history"("updated_by_id");

-- CreateIndex
CREATE INDEX "transaction_history_created_by_id_idx" ON "transaction_history"("created_by_id");

-- CreateIndex
CREATE UNIQUE INDEX "admin_roles_code_key" ON "admin_roles"("code");

-- CreateIndex
CREATE INDEX "admin_roles_created_at_idx" ON "admin_roles"("created_at");

-- CreateIndex
CREATE INDEX "admin_roles_updated_at_idx" ON "admin_roles"("updated_at");

-- CreateIndex
CREATE INDEX "admin_roles_updated_by_id_fk" ON "admin_roles"("updated_by_id");

-- CreateIndex
CREATE INDEX "admin_roles_created_by_id_fk" ON "admin_roles"("created_by_id");

-- CreateIndex
CREATE INDEX "admin_users_roles_links_inv_fk" ON "admin_users_roles_links"("role_id");

-- CreateIndex
CREATE INDEX "admin_users_roles_links_fk" ON "admin_users_roles_links"("user_id");

-- CreateIndex
CREATE UNIQUE INDEX "admin_users_roles_links_unique" ON "admin_users_roles_links"("user_id", "role_id");

-- CreateIndex
CREATE INDEX "api_token_permissions_updated_by_id_fk" ON "api_token_permissions"("updated_by_id");

-- CreateIndex
CREATE INDEX "api_token_permissions_created_by_id_fk" ON "api_token_permissions"("created_by_id");

-- CreateIndex
CREATE INDEX "api_token_permissions_token_links_inv_fk" ON "api_token_permissions_token_links"("api_token_id");

-- CreateIndex
CREATE INDEX "api_token_permissions_token_links_fk" ON "api_token_permissions_token_links"("api_token_permission_id");

-- CreateIndex
CREATE UNIQUE INDEX "api_token_permissions_token_links_unique" ON "api_token_permissions_token_links"("api_token_permission_id", "api_token_id");

-- CreateIndex
CREATE INDEX "api_tokens_updated_by_id_fk" ON "api_tokens"("updated_by_id");

-- CreateIndex
CREATE INDEX "api_tokens_created_by_id_fk" ON "api_tokens"("created_by_id");

-- CreateIndex
CREATE INDEX "transfer_token_permissions_updated_by_id_fk" ON "transfer_token_permissions"("updated_by_id");

-- CreateIndex
CREATE INDEX "transfer_token_permissions_created_by_id_fk" ON "transfer_token_permissions"("created_by_id");

-- CreateIndex
CREATE INDEX "transfer_token_permissions_token_links_inv_fk" ON "transfer_token_permissions_token_links"("transfer_token_id");

-- CreateIndex
CREATE INDEX "transfer_token_permissions_token_links_fk" ON "transfer_token_permissions_token_links"("transfer_token_permission_id");

-- CreateIndex
CREATE UNIQUE INDEX "transfer_token_permissions_token_links_unique" ON "transfer_token_permissions_token_links"("transfer_token_permission_id", "transfer_token_id");

-- CreateIndex
CREATE INDEX "transfer_tokens_updated_by_id_fk" ON "transfer_tokens"("updated_by_id");

-- CreateIndex
CREATE INDEX "transfer_tokens_created_by_id_fk" ON "transfer_tokens"("created_by_id");

-- CreateIndex
CREATE UNIQUE INDEX "static_pages_slug_key" ON "static_pages"("slug");

-- AddForeignKey
ALTER TABLE "wallets" ADD CONSTRAINT "wallets_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "wallets" ADD CONSTRAINT "wallets_tokenId_fkey" FOREIGN KEY ("tokenId") REFERENCES "ethereum_wallet"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "address_wallets" ADD CONSTRAINT "address_wallets_networkId_fkey" FOREIGN KEY ("networkId") REFERENCES "network"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "admin_users" ADD CONSTRAINT "admin_users_updated_by_id_fkey" FOREIGN KEY ("updated_by_id") REFERENCES "admin_users"("id") ON DELETE SET NULL ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "admin_users" ADD CONSTRAINT "admin_users_created_by_id_fkey" FOREIGN KEY ("created_by_id") REFERENCES "admin_users"("id") ON DELETE SET NULL ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "article" ADD CONSTRAINT "article_updated_by_id_fkey" FOREIGN KEY ("updated_by_id") REFERENCES "admin_users"("id") ON DELETE SET NULL ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "article" ADD CONSTRAINT "article_created_by_id_fkey" FOREIGN KEY ("created_by_id") REFERENCES "admin_users"("id") ON DELETE SET NULL ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "article" ADD CONSTRAINT "article_adminUserId_fkey" FOREIGN KEY ("adminUserId") REFERENCES "admin_users"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "balance_history" ADD CONSTRAINT "balance_history_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "balance_history" ADD CONSTRAINT "balance_history_ownerUsersId_fkey" FOREIGN KEY ("ownerUsersId") REFERENCES "owner_users"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "balance_history" ADD CONSTRAINT "balance_history_balanceId_fkey" FOREIGN KEY ("balanceId") REFERENCES "balance"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "notifications" ADD CONSTRAINT "notifications_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "transaction" ADD CONSTRAINT "transaction_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "transaction" ADD CONSTRAINT "transaction_ownerUsersId_fkey" FOREIGN KEY ("ownerUsersId") REFERENCES "owner_users"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "transaction_history" ADD CONSTRAINT "transaction_history_userId_fkey" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "transaction_history" ADD CONSTRAINT "transaction_history_transactionId_fkey" FOREIGN KEY ("transactionId") REFERENCES "transaction"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "transaction_history" ADD CONSTRAINT "transaction_history_ownerUsersId_fkey" FOREIGN KEY ("ownerUsersId") REFERENCES "owner_users"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "admin_roles" ADD CONSTRAINT "admin_roles_updated_by_id_fkey" FOREIGN KEY ("updated_by_id") REFERENCES "admin_users"("id") ON DELETE SET NULL ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "admin_roles" ADD CONSTRAINT "admin_roles_created_by_id_fkey" FOREIGN KEY ("created_by_id") REFERENCES "admin_users"("id") ON DELETE SET NULL ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "admin_users_roles_links" ADD CONSTRAINT "admin_users_roles_links_role_id_fkey" FOREIGN KEY ("role_id") REFERENCES "admin_roles"("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "admin_users_roles_links" ADD CONSTRAINT "admin_users_roles_links_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "admin_users"("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "api_token_permissions" ADD CONSTRAINT "api_token_permissions_updated_by_id_fkey" FOREIGN KEY ("updated_by_id") REFERENCES "admin_users"("id") ON DELETE SET NULL ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "api_token_permissions" ADD CONSTRAINT "api_token_permissions_created_by_id_fkey" FOREIGN KEY ("created_by_id") REFERENCES "admin_users"("id") ON DELETE SET NULL ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "api_token_permissions_token_links" ADD CONSTRAINT "api_token_permissions_token_links_api_token_id_fkey" FOREIGN KEY ("api_token_id") REFERENCES "api_tokens"("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "api_token_permissions_token_links" ADD CONSTRAINT "api_token_permissions_token_links_api_token_permission_id_fkey" FOREIGN KEY ("api_token_permission_id") REFERENCES "api_token_permissions"("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "api_tokens" ADD CONSTRAINT "api_tokens_updated_by_id_fkey" FOREIGN KEY ("updated_by_id") REFERENCES "admin_users"("id") ON DELETE SET NULL ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "api_tokens" ADD CONSTRAINT "api_tokens_created_by_id_fkey" FOREIGN KEY ("created_by_id") REFERENCES "admin_users"("id") ON DELETE SET NULL ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "transfer_token_permissions" ADD CONSTRAINT "transfer_token_permissions_updated_by_id_fkey" FOREIGN KEY ("updated_by_id") REFERENCES "admin_users"("id") ON DELETE SET NULL ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "transfer_token_permissions" ADD CONSTRAINT "transfer_token_permissions_created_by_id_fkey" FOREIGN KEY ("created_by_id") REFERENCES "admin_users"("id") ON DELETE SET NULL ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "transfer_token_permissions_token_links" ADD CONSTRAINT "transfer_token_permissions_token_links_transfer_token_id_fkey" FOREIGN KEY ("transfer_token_id") REFERENCES "transfer_tokens"("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "transfer_token_permissions_token_links" ADD CONSTRAINT "transfer_token_permissions_token_links_transfer_token_perm_fkey" FOREIGN KEY ("transfer_token_permission_id") REFERENCES "transfer_token_permissions"("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "transfer_tokens" ADD CONSTRAINT "transfer_tokens_updated_by_id_fkey" FOREIGN KEY ("updated_by_id") REFERENCES "admin_users"("id") ON DELETE SET NULL ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "transfer_tokens" ADD CONSTRAINT "transfer_tokens_created_by_id_fkey" FOREIGN KEY ("created_by_id") REFERENCES "admin_users"("id") ON DELETE SET NULL ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "static_pages" ADD CONSTRAINT "static_pages_updated_by_id_fkey" FOREIGN KEY ("updated_by_id") REFERENCES "admin_users"("id") ON DELETE SET NULL ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE "static_pages" ADD CONSTRAINT "static_pages_created_by_id_fkey" FOREIGN KEY ("created_by_id") REFERENCES "admin_users"("id") ON DELETE SET NULL ON UPDATE NO ACTION;
