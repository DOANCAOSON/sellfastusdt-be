import { MailerModule } from '@nestjs-modules/mailer'
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter'
import { CacheInterceptor, CacheModule } from '@nestjs/cache-manager'
import {
  Injectable,
  Logger,
  MiddlewareConsumer,
  Module,
  NestMiddleware,
  NestModule,
  RequestMethod
} from '@nestjs/common'
import { APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core'
import { ScheduleModule } from '@nestjs/schedule'
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler'
import { FastifyReply, FastifyRequest } from 'fastify'
import { parse } from 'qs'
import { CmsModule } from './modules/cms/cms.module'
import { GatewayModule } from './modules/gateway/gateway.module'
import { LandingModule } from './modules/landing/landing.module'
import { StaticController } from './modules/static/static.controller'

@Injectable()
class LoggerMiddleware implements NestMiddleware {
  private readonly logger = new Logger(LoggerMiddleware.name)
  use(req: FastifyRequest, res: FastifyReply['raw'], next: () => void) {
    const start = Date.now()
    res.on('close', () => {
      this.logger.log(`${req.method} ${req.originalUrl} (${Date.now() - start} ms) ${res.statusCode}`)
    })
    if (next) {
      next()
    }
  }
}

@Injectable()
class PaginationMiddleware implements NestMiddleware {
  use(req: FastifyRequest, _: FastifyReply['raw'], next: () => any) {
    if (req.originalUrl.match(/find/)) {
      const query = parse(req.originalUrl.split('?')[1]) as { page: string | number; pageSize: string | number }
      const { page, pageSize } = query
      query.page = page && +page > 0 ? +page : 1
      query.pageSize = pageSize && +pageSize > 0 ? +pageSize : 10
      Object.assign(req.query as any, query)
    } else if (req.originalUrl.match(/dashboard/)) {
      const query = parse(req.originalUrl.split('?')[1])
      Object.assign(req.query as any, query)
    }
    next?.()
  }
}

@Module({
  imports: [
    CmsModule,
    LandingModule,
    GatewayModule,
    ScheduleModule.forRoot(),
    CacheModule.register({ isGlobal: true, ttl: 10000 }),
    MailerModule.forRoot({
      transport: 'smtps://user@domain.com:pass@smtp.domain.com',
      template: {
        dir: process.cwd() + '/templates/',
        adapter: new HandlebarsAdapter(),
        options: {
          strict: true
        }
      }
    }),
    ThrottlerModule.forRoot([
      {
        name: 'short',
        ttl: 100,
        limit: 3
      },
      {
        name: 'medium',
        ttl: 1000,
        limit: 20
      },
      {
        name: 'long',
        ttl: 5000,
        limit: 100
      }
    ])
  ],
  providers: [
    { provide: APP_INTERCEPTOR, useClass: CacheInterceptor },
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard
    }
  ],
  controllers: [StaticController]
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes({ path: '*', method: RequestMethod.ALL })
    consumer.apply(PaginationMiddleware).forRoutes({ path: '*', method: RequestMethod.GET })
  }
}
