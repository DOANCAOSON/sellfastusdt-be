import { CacheInterceptor } from '@nestjs/cache-manager'
import { Controller, Get, Query, UseInterceptors } from '@nestjs/common'
import { DashboardQueryDto } from './dashboard.dto'
import { DashboardService } from './dashboard.service'

@UseInterceptors(CacheInterceptor)
@Controller('cms/dashboard')
export class DashboardController {
  constructor(private readonly dashboardService: DashboardService) {}

  @Get('summary')
  findSummary(@Query() query: DashboardQueryDto) {
    return this.dashboardService.findSummary(query)
  }

  @Get('transaction-analytics')
  findOrderAnalytics(@Query() query: DashboardQueryDto) {
    return this.dashboardService.findTransactionAnalytics(query)
  }

  @Get('transaction-stats')
  findOrderStats(@Query() query: DashboardQueryDto) {
    return this.dashboardService.findTransactionStats(query)
  }
}
