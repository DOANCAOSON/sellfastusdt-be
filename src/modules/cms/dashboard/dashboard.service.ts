import { BaseService } from '@/modules/base/service'
import { addDateToStr, dateToStr, diff, strToDate, toCurrency } from '@/modules/base/util'
import { Injectable } from '@nestjs/common'
import { DashboardQueryDto } from './dashboard.dto'

@Injectable()
export class DashboardService extends BaseService {
  async findTransactionStats(query: DashboardQueryDto) {
    const where = this.withFilterDate(query)
    const dataSource = await this._prisma.transaction.findMany({ select: { status: true }, where })

    const _map: Map<string | null, number> = new Map()
    dataSource.forEach((d) => {
      if (_map.has(d.status)) {
        const currentVal = _map.get(d.status) || 1
        _map.set(d.status, currentVal + 1)
      } else {
        _map.set(d.status, 1)
      }
    })

    const series: number[] = []
    const labels: (string | null)[] = []
    for (let [key, value] of _map.entries()) {
      labels.push(key)
      series.push(value)
    }

    return { series, labels }
  }

  async findTransactionAnalytics(query: DashboardQueryDto) {
    const where = this.withFilterDate(query)
    const dataSource = await this._prisma.transaction.findMany({
      select: { ethereumAmount: true, createdAt: true },
      where,
      orderBy: { createdAt: 'asc' }
    })

    const _map = this.withLabels(query)
    dataSource.forEach((d) => {
      const date = dateToStr(d.createdAt)
      if (_map.has(date)) {
        const [total, amount] = _map.get(date) || [1, 0]
        _map.set(date, [total + 1, amount + (d.ethereumAmount || 0)])
      } else {
        _map.set(date, [1, d.ethereumAmount || 0])
      }
    })

    const totalTransactions: number[] = []
    const totalAmount: number[] = []
    const labels: (string | null)[] = []
    for (let [key, [total, amount]] of _map.entries()) {
      labels.push(key)
      totalTransactions.push(total)
      totalAmount.push(amount)
    }

    return {
      labels,
      series: [
        { name: 'Total Transactions', type: 'column', data: totalTransactions },
        { name: 'Total Amount', type: 'line', data: totalAmount }
      ]
    }
  }

  async findSummary(query: DashboardQueryDto) {
    const where = this.withFilterDate(query)
    const [totalTransaction, totalAmountRs, totalUser] = await Promise.all([
      this._prisma.transaction.count({ where }),
      this._prisma.transaction.aggregate({ where, _sum: { ethereumAmount: true } }),
      this._prisma.transaction.findMany({ where, select: { userId: true }, distinct: 'userId' })
    ])
    return {
      transaction: { label: 'Total Transactions', value: totalTransaction },
      amount: { label: 'Total Amount', value: toCurrency(totalAmountRs._sum.ethereumAmount || 0) },
      user: { label: 'Total Customers', value: totalUser.length }
    }
  }

  private withFilterDate(query: DashboardQueryDto): { AND?: Array<any> } {
    const where = {}

    const AND: any[] = []
    const range = Object.values(query).map((date) => date) as [string, string]
    AND.push({
      ['createdAt']: { gte: strToDate(range[0], 'start'), lte: strToDate(range[1], 'end') }
    })

    where['AND'] = AND
    return where
  }

  private withLabels(query: DashboardQueryDto): Map<string | null, [number, number]> {
    const _map: Map<string | null, [number, number]> = new Map()
    const range = Object.values(query).map((date) => date) as [string, string]
    const total = diff(range[1], 'day', range[0])
    _map.set(range[0], [0, 0])
    for (let index = 1; index < total; index++) {
      _map.set(addDateToStr(range[0], index, 'day'), [0, 0])
    }
    _map.set(range[1], [0, 0])
    return _map
  }
}
