import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const AdminRoleSchema = z.object({
  name: z.string(),
  code: z.string().nullable().optional(),
  description: z.string().nullable().optional(),
  permissions: z
    .object({
      collection: z.string(),
      create: z.boolean(),
      read: z.boolean(),
      update: z.boolean(),
      delete: z.boolean()
    })
    .array()
})

export class CreateAdminRoleDto extends createZodDto(AdminRoleSchema) {}

export class UpdateAdminRoleDto extends CreateAdminRoleDto {}

const AdminUserSchema = z.object({
  firstname: z.string(),
  lastname: z.string(),
  email: z.string(),
  password: z.string(),
  roles: z.string().array(),
  isActive: z.boolean().nullable().optional()
})
export class CreateAdminUserDto extends createZodDto(AdminUserSchema) {}

const UpdateAdminUserSchema = z.object({
  firstname: z.string().nullable().optional(),
  lastname: z.string().nullable().optional(),
  email: z.string().nullable().optional(),
  password: z.string().nullable().optional(),
  roles: z.string().array().nullable().optional(),
  isActive: z.boolean().nullable().optional()
})

export class UpdateAdminUserDto extends createZodDto(UpdateAdminUserSchema) {}

const UpdateUserProfileSchema = z.object({
  firstname: z.string().nullable().optional(),
  lastname: z.string().nullable().optional(),
  password: z.string().nullable().optional()
})

export class UpdateUserProfileDto extends createZodDto(UpdateUserProfileSchema) {}
