import { BaseDto, CurrentUser, IListDto, IListQuery } from '@/modules/base/dto'
import { BaseService } from '@/modules/base/service'
import { toId, toSlug } from '@/modules/base/util'
import { Injectable, NotFoundException } from '@nestjs/common'
import { hashSync } from 'bcryptjs'
import {
  CreateAdminRoleDto,
  CreateAdminUserDto,
  UpdateAdminRoleDto,
  UpdateAdminUserDto,
  UpdateUserProfileDto
} from './admin.dto'

const menu = [
  { key: '/', label: 'Dashboard', path: '/' },
  { key: 'users', label: 'Users', path: '/users', viewable: true, creatable: true, exportable: true },
  { key: 'articles', label: 'Articles', path: '/articles', viewable: true, creatable: true, exportable: false },
  {
    key: 'transactions',
    label: 'Transactions',
    path: '/transactions',
    viewable: true,
    creatable: true,
    exportable: true
  },
  {
    key: 'balances',
    label: 'Balances',
    path: '/balances',
    viewable: true,
    exportable: true
  }
]

const STATIC_PAGES_MENU = [
  { key: 'static-pages/fit-guide', label: 'Fit Guide', path: '/static-pages/fit-guide' },
  { key: 'static-pages/about-us', label: 'About Us', path: '/static-pages/about-us' },
  { key: 'static-pages/contact-us', label: 'Contact Us', path: '/static-pages/contact-us' },
  { key: 'static-pages/telegram', label: 'Telegram', path: 'static-pages/telegram', textarea: true },
  { key: 'static-pages/promotion', label: 'Promotion', path: 'static-pages/promotion', textarea: true }
]

const SUPER_ADMIN_MENU = [
  { key: 'admin/users', label: 'Admin User', path: '/admin/users', viewable: true, creatable: true, exportable: true },
  {
    key: 'admin/articles',
    label: 'Admin Articles',
    path: '/admin/articles',
    viewable: true,
    creatable: true,
    exportable: true
  },
  {
    key: 'admin/transactions',
    label: 'Admin Transactions',
    path: '/admin/transactions',
    viewable: true,
    creatable: true,
    exportable: true,
    notify: true
  },
  { key: 'admin/roles', label: 'Admin Role', path: '/admin/roles', viewable: true, creatable: true, exportable: true }
]

const CONFIGURATION_MENU = [
  { key: 'tokens', label: 'Tokens', path: '/tokens' },
  { key: 'networks', label: 'Networks', path: '/networks' }
]

const FULL_PERMISSIONS = [
  { key: 'User', read: true, create: true, delete: true, update: true, collection: 'User' },
  { key: 'Article', read: true, create: true, delete: true, update: true, collection: 'Article' },
  { key: 'Transaction', read: true, create: true, delete: true, update: true, collection: 'Transaction' }
]

@Injectable()
export class AdminService extends BaseService {
  findUserMenu(user: CurrentUser) {
    // TODO check user permission
    if (user.roles.includes('super-admin')) {
      return menu.concat(SUPER_ADMIN_MENU).concat(STATIC_PAGES_MENU).concat(CONFIGURATION_MENU)
    }
    return menu.concat(STATIC_PAGES_MENU)
  }

  async createAdminUser(input: CreateAdminUserDto) {
    const { password, roles, ...data } = input
    const count = await this._prisma.adminUser.count({ where: { email: data.email } })
    if (count >= 1) {
      return this.withBadRequest(['email: Already used.'])
    } else {
      return this._prisma.adminUser.create({
        data: {
          id: toId(),
          password: hashSync(password),
          ...data,
          adminUserRoleLinks: {
            createMany: { data: roles.map((roleId) => ({ roleId, id: toId() })), skipDuplicates: true }
          }
        },
        select: { id: true }
      })
    }
  }

  async findAdminUsers(query: IListQuery): Promise<IListDto> {
    const { skip, take } = this.withLimitOffset(query)
    const [results, total] = await Promise.all([
      this._prisma.adminUser.findMany({
        select: {
          id: true,
          isActive: true,
          email: true,
          firstname: true,
          lastname: true,
          createdAt: true,
          adminUserRoleLinks: { select: { adminRole: { select: { name: true, code: true } } } }
        },
        where: {},
        orderBy: { createdAt: 'desc' },
        take,
        skip
      }),
      this._prisma.adminUser.count()
    ])

    const dataSource = results.map(({ adminUserRoleLinks, ...rest }) => {
      rest['key'] = rest.id
      rest['roles'] = adminUserRoleLinks.map((r) => r.adminRole?.name).join(', ')
      return rest
    })
    return this.withPagination(dataSource, query, total)
  }

  async findAdminUser(id: string) {
    const user = await this._prisma.adminUser.findFirst({
      where: { id },
      select: {
        id: true,
        email: true,
        firstname: true,
        lastname: true,
        isActive: true,
        adminUserRoleLinks: { select: { roleId: true } }
      }
    })

    if (!user) throw new NotFoundException()

    const { adminUserRoleLinks, ...output } = user
    output['roles'] = adminUserRoleLinks.map((x) => x.roleId)
    return output
  }

  async updateAdminUser(id: string, input: UpdateAdminUserDto) {
    const count = await this._prisma.adminUser.count({ where: { id } })
    if (count === 0) throw new NotFoundException()

    const { password, roles, ...data } = input
    if (password) {
      data['password'] = hashSync(password)
    }

    return this._prisma.$transaction(
      async (tx) => {
        if (roles && roles.length > 0) {
          await tx.adminUserRoleLink.deleteMany({ where: { userId: id } })
          await tx.adminUserRoleLink.createMany({ data: roles.map((r) => ({ roleId: r, userId: id, id: toId() })) })
        }
        return this._prisma.adminUser.update({ where: { id }, data, select: { id: true } })
      },
      { isolationLevel: 'ReadCommitted', maxWait: 10000, timeout: 10000 }
    )
  }

  async updateUserProfile(id: string, input: UpdateUserProfileDto) {
    const count = await this._prisma.adminUser.count({ where: { id } })
    if (count === 0) throw new NotFoundException()

    const { password, ...data } = input
    if (password) {
      data['password'] = hashSync(password)
    }

    return this._prisma.adminUser.update({ where: { id }, data, select: { updatedAt: true } })
  }

  async createAdminRole(input: CreateAdminRoleDto & BaseDto) {
    const code = toSlug(input.name)
    const count = await this._prisma.adminRole.count({ where: { code } })
    if (count >= 1) {
      return this.withBadRequest(['name: Already used.'])
    } else {
      input.code = code
      input.id = toId()
      return this._prisma.adminRole.create({ data: input, select: { id: true } })
    }
  }

  async findAdminRoles(query: IListQuery): Promise<IListDto> {
    let { skip, take } = this.withLimitOffset(query)
    const [dataSource, total] = await Promise.all([
      this._prisma.adminRole.findMany({
        select: { id: true, name: true, code: true, description: true, createdAt: true },
        where: {},
        orderBy: { createdAt: 'desc' },
        take,
        skip
      }),
      this._prisma.adminRole.count()
    ])

    dataSource.forEach((x: any) => {
      x.key = x.id
      x.deletable = x.code !== 'super-admin'
    })
    return this.withPagination(dataSource, query, total)
  }

  async findAdminRelation() {
    const data = await this._prisma.adminRole.findMany({
      select: { id: true, name: true, code: true }
    })
    return data.map((d) => ({ value: d.id, label: d.name, disabled: d.code === 'super-admin' }))
  }

  async findAdminNetworks() {
    const data = await this._prisma.network.findMany({
      select: { id: true, name: true }
    })
    return data.map((d) => ({ value: d.id, label: d.name }))
  }

  async findAdminRole(id: string) {
    const role = await this._prisma.adminRole.findFirst({
      where: { id },
      select: { id: true, name: true, code: true, description: true, permissions: true }
    })

    if (!role) throw new NotFoundException()

    if (role.code === 'super-admin') {
      role.permissions = FULL_PERMISSIONS
    }
    return role
  }

  updateAdminRole(id: string, input: UpdateAdminRoleDto) {
    return this._prisma.adminRole.update({ where: { id }, data: input, select: { id: true } })
  }

  async removeAdminRole(id: string) {
    const count = await this._prisma.adminRole.count({ where: { id } })
    if (count === 0) throw new NotFoundException()

    return this._prisma.$transaction(
      async (tx) => {
        await tx.adminUserRoleLink.deleteMany({ where: { roleId: id } })
        return this._prisma.adminRole.delete({ where: { id }, select: { id: true } })
      },
      { isolationLevel: 'ReadCommitted', maxWait: 10000, timeout: 10000 }
    )
  }
}
