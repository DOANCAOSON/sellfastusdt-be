import { BaseController } from '@/modules/base/controller'
import { IListQuery } from '@/modules/base/dto'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Delete, Get, Param, Patch, Post, Query, Req, UseInterceptors } from '@nestjs/common'
import { ArticleDto } from './article.dto'
import { ArticlesService } from './article.service'

@UseInterceptors(CacheInterceptor)
@Controller('cms/articles')
export class ArticlesController extends BaseController {
  constructor(private readonly articlesService: ArticlesService) {
    super()
  }

  @Post()
  create(@Body() body: ArticleDto, @Req() req: Request) {
    return this.articlesService.create(this.withUpdated(body, req))
  }

  @Get('find')
  findMany(@Query() query: IListQuery, @Req() req: Request) {
    return this.articlesService.findAll(query, req['user'])
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.articlesService.findOne(id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() body: ArticleDto, @Req() req: Request) {
    return this.articlesService.update(id, this.withUpdated(body, req))
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.articlesService.remove(id)
  }
}
