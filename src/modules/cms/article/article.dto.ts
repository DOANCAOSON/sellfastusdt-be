import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const ArticleSchema = z.object({
  title: z.string().max(100),
  slug: z.string(),
  images: z.object({ thumbUrl: z.string(), name: z.string(), type: z.string() }).array().max(10).nullable().optional(),
  description: z.string().max(100).optional().nullable(),
  body: z.string(),
  published: z.boolean()
})
export class ArticleDto extends createZodDto(ArticleSchema) {}
