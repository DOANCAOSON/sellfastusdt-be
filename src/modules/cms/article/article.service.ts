import { BaseDto, CurrentUser, IFilter, IListQuery } from '@/modules/base/dto'
import { removeImageFile, saveImageFile, toId, toSlug } from '@/modules/base/util'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Injectable, NotFoundException, UseInterceptors } from '@nestjs/common'
import { readFileSync } from 'fs'
import { join } from 'path'
import { BaseService } from '../../base/service'
import { ArticleDto } from './article.dto'

@UseInterceptors(CacheInterceptor)
@Injectable()
export class ArticlesService extends BaseService {
  private readonly filterable: Array<IFilter>
  constructor() {
    super()
    const fileData = readFileSync(join(process.cwd(), '/prisma/schemas/article.json'), 'utf-8')
    const schema = JSON.parse(fileData.toString())
    this.filterable = schema['layouts'].filterable.flat()
  }

  async create(input: ArticleDto) {
    const { images: imagesUpload, ...data } = input

    if (imagesUpload) {
      const images = await saveImageFile(imagesUpload, 'product')
      data['images'] = images.join(',')
      data['imagesUpload'] = imagesUpload
    }

    return this._prisma.$transaction(
      async (tx) => {
        data['slug'] = await this.generateSlug(data.title)
        return tx.article.create({
          data: {
            ...data,
            id: toId()
          },
          select: { id: true }
        })
      },
      { isolationLevel: 'ReadCommitted', maxWait: 10000, timeout: 10000 }
    )
  }

  findDrafts() {
    return this._prisma.article.findMany({ where: { published: false } })
  }

  async findAll(query: IListQuery, user: CurrentUser) {
    const { skip, take } = this.withLimitOffset(query)
    const where = this.withFilter(query, this.filterable)

    const [dataSource, total] = await Promise.all([
      this._prisma.article.findMany({
        select: {
          id: true,
          title: true,
          description: true,
          body: true,
          createdAt: true,
          published: true,
          images: true,
          createdBy: { select: { id: true } }
        },
        where,
        orderBy: { createdAt: 'desc' },
        take,
        skip
      }),
      this._prisma.article.count({ where })
    ])
    const isSuperAdmin = user.roles.includes('super-admin')

    const data = dataSource.map((d) => {
      const { createdBy } = d
      d['key'] = d.id
      d['deletable'] = isSuperAdmin || createdBy?.id === user.id
      return d
    })
    return this.withPagination(data, query, total)
  }

  async findOne(id: string) {
    const article = await this._prisma.article.findFirst({
      where: { id },
      select: {
        title: true,
        id: true,
        createdAt: true,
        published: true,
        slug: true,
        description: true,
        body: true,
        images: true,
        updatedAt: true,
        imagesUpload: true
      }
    })
    if (!article) throw new NotFoundException()
    const { imagesUpload } = article
    article['images'] = imagesUpload as any
    return article
  }

  async update(id: string, input: Partial<ArticleDto & BaseDto>) {
    const { images: imagesUpload, ...data } = input

    const detail = await this._prisma.article.findFirst({
      where: { id },
      select: {
        imagesUpload: true
      }
    })
    if (!detail) {
      throw new NotFoundException()
    }

    if (imagesUpload) {
      const images = await saveImageFile(imagesUpload, 'article')
      data['images'] = images.join(',')
      data['imagesUpload'] = imagesUpload
    }
    return this._prisma.$transaction(
      async (tx) => {
        return tx.article.update({
          where: { id },
          data,
          select: { id: true }
        })
      },
      { isolationLevel: 'ReadCommitted', maxWait: 10000, timeout: 10000 }
    )
  }

  async remove(id: string) {
    const article = await this._prisma.article.findFirst({
      where: { id },
      select: { images: true }
    })
    if (!article) throw new NotFoundException()

    const output = await this._prisma.$transaction(
      async (tx) => {
        await Promise.all([tx.article.delete({ where: { id: id } })])

        return { id }
      },
      { isolationLevel: 'ReadCommitted', maxWait: 10000, timeout: 10000 }
    )

    setTimeout(() => {
      removeImageFile(article.images)
    }, 0)

    return output
  }

  private async generateSlug(name: string) {
    let slug = toSlug(name)

    const count = await this._prisma.article.count({ where: { slug } })
    if (count > 0) {
      const count = await this._prisma.article.count()
      slug = `${slug}-${count + 1}`
    }

    return slug
  }
}
