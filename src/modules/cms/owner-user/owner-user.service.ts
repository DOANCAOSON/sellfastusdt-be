import type { BaseDto, CurrentUser, IFilter, IListQuery } from '@/modules/base/dto'
import { toId } from '@/modules/base/util'
import { Injectable, NotFoundException } from '@nestjs/common'
import { readFileSync } from 'fs'
import { join } from 'path'
import { BaseService } from '../../base/service'
import { CreateOwnerUserBankDto } from './owner-user.dto'

@Injectable()
export class OwnerUserService extends BaseService {
  private readonly filterable: Array<IFilter>
  constructor() {
    super()
    const fileData = readFileSync(join(process.cwd(), '/prisma/schemas/owner-user.json'), 'utf-8')
    const schema = JSON.parse(fileData.toString())
    this.filterable = schema['layouts'].filterable.flat()
  }

  async findMany(query: IListQuery, user: CurrentUser) {
    const { skip, take } = this.withLimitOffset(query)
    const where = this.withFilter(query, this.filterable)

    const [dataSource, addressWallet, total] = await Promise.all([
      this._prisma.ownerUsers.findMany({
        select: {
          id: true,
          email: true,
          createdAt: true,
          isActive: true,
          TransactionHistory: true
        },
        where,
        orderBy: { createdAt: 'desc' },
        take,
        skip
      }),
      this._prisma.addressWallet.findMany({
        select: {
          id: true
        },
        where,
        orderBy: { createdAt: 'desc' },
        take,
        skip
      }),
      this._prisma.ownerUsers.count({ where })
    ])

    const isSuperAdmin = user.roles.includes('super-admin')

    dataSource.forEach((d) => {
      d['key'] = d.id
      d['deletable'] = isSuperAdmin
      d['addressWallet'] = addressWallet.length
      d['countTransaction'] = d.TransactionHistory.length.toString()
    })

    return this.withPagination(dataSource, query, total)
  }

  async findOne(id: string) {
    const [addresses, ownerUser] = await Promise.all([
      this._prisma.addressWallet.findMany({
        select: {
          userId: true,
          addressWallet: true,
          networkId: true,
          network: true
        },
        where: { userId: id }
      }),
      this._prisma.ownerUsers.findFirst({
        where: { id },
        select: {
          id: true,
          email: true,
          createdAt: true,
          isActive: true
        }
      })
    ])

    if (!ownerUser) {
      throw new NotFoundException()
    }
    return { ...ownerUser, addresses }
  }

  async update(id: string, input: Partial<CreateOwnerUserBankDto & BaseDto>) {
    const { addresses, email, isActive, ...data } = input

    const ownerUser = await this._prisma.ownerUsers.findFirst({
      where: { id },
      select: {
        id: true
      }
    })
    if (isActive) {
      await this._prisma.ownerUsers.updateMany({
        where: { isActive: true },
        data: {
          isActive: false
        }
      })
    }
    if (!ownerUser) {
      throw new NotFoundException()
    }

    await Promise.all([
      this._prisma.ownerUsers.update({
        where: { id: ownerUser.id },
        data: {
          email,
          isActive
        }
      })
    ])
    return { id }
  }

  async createUserAdmin(body: CreateOwnerUserBankDto) {
    const { email, isActive, addresses } = body

    if (isActive) {
      return this.withBadRequest(['isActive: If you create a new one, you cannot set active for this owner'])
    }

    const ownerUser = await this._prisma.ownerUsers.create({
      data: { email, id: toId(), isActive },
      select: {
        id: true,
        createdAt: true
      }
    })

    const addressRequest =
      addresses?.map((ad) =>
        this._prisma.addressWallet.create({
          data: {
            id: toId(),
            addressWallet: ad.address,
            userId: ownerUser.id,
            networkId: ad.networkId
          },
          select: {
            id: true
          }
        })
      ) || []

    const res = (await Promise.all([...addressRequest])).map((a) => a.id)

    return res
  }

  async remove(id: string) {
    const ownerUser = await this._prisma.ownerUsers.delete({
      where: { id },
      select: { id: true }
    })

    return ownerUser
  }
}
