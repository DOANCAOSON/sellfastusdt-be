import { BaseController } from '@/modules/base/controller'
import type { IListQuery } from '@/modules/base/dto'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Delete, Get, Param, Patch, Post, Query, Req, UseInterceptors } from '@nestjs/common'
import { CreateOwnerUserBankDto } from './owner-user.dto'
import { OwnerUserService } from './owner-user.service'

@UseInterceptors(CacheInterceptor)
@Controller('cms/owner-users')
export class OwnerUserController extends BaseController {
  constructor(private readonly userService: OwnerUserService) {
    super()
  }

  @Get('find')
  findMany(@Query() query: IListQuery, @Req() req: Request) {
    return this.userService.findMany(query, req['user'])
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.userService.findOne(id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() body: CreateOwnerUserBankDto, @Req() req: Request) {
    return this.userService.update(id, this.withUpdated(body, req))
  }

  @Post()
  patchAdmin(@Body() body: CreateOwnerUserBankDto) {
    return this.userService.createUserAdmin(body)
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.userService.remove(id)
  }
}
