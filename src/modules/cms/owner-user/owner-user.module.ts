import { Module } from '@nestjs/common'
import { OwnerUserController } from './owner-user.controller'
import { OwnerUserService } from './owner-user.service'

@Module({
  controllers: [OwnerUserController],
  providers: [OwnerUserService]
})
export class OwnerUserModule {}
