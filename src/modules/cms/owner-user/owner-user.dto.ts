import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const CreateSchema = z.object({
  email: z.string().max(100),
  addresses: z.any().array(),
  isActive: z.boolean()
})
export class CreateOwnerUserBankDto extends createZodDto(CreateSchema) {}
