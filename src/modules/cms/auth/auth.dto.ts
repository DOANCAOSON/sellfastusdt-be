import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const SignInSchema = z.object({ email: z.string().min(1), password: z.string().min(1) })

export class CredentialDto extends createZodDto(SignInSchema) {}
