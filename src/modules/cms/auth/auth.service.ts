import { Injectable } from '@nestjs/common'
import { compareSync } from 'bcryptjs'
import { BaseService } from '../../base/service'

@Injectable()
export class AuthService extends BaseService {
  async checkCredentials(email: string, password: string) {
    const user = await this._prisma.adminUser.findUnique({
      where: { email, isActive: true },
      select: {
        id: true,
        email: true,
        password: true,
        firstname: true,
        lastname: true,
        isActive: true,
        adminUserRoleLinks: { select: { adminRole: { select: { code: true } } } }
      }
    })

    if (user) {
      const isValid = this.validatePassword(password, user.password)
      const { adminUserRoleLinks, ...output } = user
      output['roles'] = adminUserRoleLinks.map((r) => r.adminRole?.code)
      return isValid ? output : null
    }
    return user
  }

  private validatePassword(password: string, hashed: string | null) {
    return compareSync(password, hashed || '')
  }
}
