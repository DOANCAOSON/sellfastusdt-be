import { Module } from '@nestjs/common'
import { JwtModule } from '@nestjs/jwt'
import { AdminModule } from './admin/admin.module'
import { ArticlesModule } from './article/article.module'
import { AuthModule } from './auth/auth.module'
import { BalanceModule } from './balance/balance.module'
import { DashboardModule } from './dashboard/dashboard.module'
import { NetworksModule } from './network/network.module'
import { OwnerUserModule } from './owner-user/owner-user.module'
import { StaticPagesModule } from './static-pages/static-pages.module'
import { TokensModule } from './token/token.module'
import { TransactionModule } from './transaction/transaction.module'
import { UserModule } from './user/user.module'

@Module({
  imports: [
    AuthModule,
    AdminModule,
    UserModule,
    StaticPagesModule,
    ArticlesModule,
    TransactionModule,
    NetworksModule,
    BalanceModule,
    DashboardModule,
    TokensModule,
    OwnerUserModule,
    JwtModule.register({
      global: true,
      secret: process.env.SECRET_TOKEN || 'SECRET_TOKEN',
      signOptions: { expiresIn: '60d' }
    })
  ]
  // providers: [{ provide: APP_GUARD, useClass: CMSAuthGuard }]
})
export class CmsModule {}
