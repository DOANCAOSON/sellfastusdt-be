import { BaseDto, IFilter, IListDto, IListQuery } from '@/modules/base/dto'
import { toId } from '@/modules/base/util'
import { Injectable, NotFoundException } from '@nestjs/common'
import { readFileSync } from 'fs'
import { join } from 'path'
import { BaseService } from '../../base/service'
import { TransactionStatus } from '../transaction/transaction.dto'
import { UpdateBalanceDto } from './balance.dto'

@Injectable()
export class BalancesService extends BaseService {
  private readonly filterable: Array<IFilter>
  constructor() {
    super()
    const fileData = readFileSync(join(process.cwd(), '/prisma/schemas/balance.json'), 'utf-8')
    const schema = JSON.parse(fileData.toString())
    this.filterable = schema['layouts'].filterable.flat()
  }

  async findMany(query: IListQuery & { source?: string }): Promise<IListDto> {
    const { skip, take } = this.withLimitOffset(query)
    const where = this.withFilter(query, this.filterable)
    const [dataSource, total] = await Promise.all([
      this._prisma.balance.findMany({
        select: {
          id: true,
          createdAt: true,
          status: true,
          userId: true,
          type: true,
          addressClientId: true,
          addressOwnerId: true,
          amount: true,
          code: true,
          isActive: true,
          symbolId: true,
          ownerId: true
        },
        where,
        orderBy: { createdAt: 'desc' },
        take,
        skip
      }),
      this._prisma.transaction.count({ where })
    ])

    const data: any = []
    for (const d of dataSource) {
      const symbol = await this._prisma.token.findFirst({
        where: { id: d.symbolId },
        select: { tokenName: true, images: true }
      })
      d['key'] = d.id
      d['amount'] = d.amount.toString() as any
      d['images'] = symbol?.images
      d['tokenName'] = symbol?.tokenName
      data.push(d)
    }
    return this.withPagination(data, query, total)
  }
  async findOne(id: string) {
    const detail = await this._prisma.balance.findFirst({
      select: {
        id: true,
        createdAt: true,
        status: true,
        userId: true,
        code: true,
        type: true,
        addressClientId: true,
        addressOwnerId: true,
        amount: true,
        isActive: true,
        symbolId: true,
        ownerId: true,
        netWorkId: true,
        createdById: true
      },
      where: { id }
    })
    const [owner, user, token, network] = await Promise.all([
      this._prisma.ownerUsers.findFirst({ where: { id: detail?.ownerId ?? '' } }),
      this._prisma.user.findFirst({ where: { id: detail?.userId ?? '' } }),
      this._prisma.token.findFirst({ where: { id: detail?.symbolId } }),
      this._prisma.network.findFirst({ where: { id: detail?.netWorkId ?? '' } })
    ])
    if (!detail) throw new NotFoundException()

    detail['createdBy'] = user?.email as any
    const userOwner = owner ? { id: owner.id, label: 'User Pain', path: '/owner-users' } : {}
    const userUser = user ? { id: user.id, label: 'User Received', path: '/users' } : {}

    const [userAddress, ownerAddress] = await Promise.all([
      this._prisma.addressWallet.findFirst({ where: { id: detail?.addressClientId ?? '' } }),
      this._prisma.addressWallet.findFirst({ where: { id: detail?.addressOwnerId ?? '' } })
    ])

    detail['User'] = { ...user, id: userUser, address: userAddress?.addressWallet } as any
    detail['Token'] = token as any
    detail['OwnerUsers'] = { ...owner, id: userOwner, address: ownerAddress?.addressWallet } as any
    detail['addressWallet'] = userAddress?.addressWallet
    detail['networkId'] = userAddress?.networkId
    detail['networkName'] = network?.name
    return detail
  }

  async update(id: string, input: UpdateBalanceDto & BaseDto) {
    const [balance, coinDeposit, user] = await Promise.all([
      this._prisma.balance.findFirst({
        where: { id },
        select: { status: true, id: true, type: true, userId: true, ownerId: true }
      }),
      this._prisma.token.findFirst({
        where: { id: input.symbolId },
        select: { id: true, binance: true, fee: true }
      }),
      this._prisma.user.findFirst({
        where: { id: input.userId },
        select: { id: true }
      })
    ])

    if (!balance || !coinDeposit || !user) {
      return this.withBadRequest('balance is cancelled')
    }
    const wallet = await this._prisma.wallet.findFirst({
      where: { userId: input.userId, binance: coinDeposit?.binance as any }
    })

    const data: any = { status: input.status, amount: input.amount }

    if (
      (balance?.status === TransactionStatus.CONFIRMED || balance?.status === TransactionStatus.CANCELLED) &&
      data.status === TransactionStatus.PAID
    ) {
      return this.withBadRequest('status: balance is paid')
    }

    if (input.status === TransactionStatus.CONFIRMED || input.status === TransactionStatus.CANCELLED) {
      data[input.status === TransactionStatus.CONFIRMED ? 'confirmedAt' : 'cancelAt'] = input.updatedAt
    }

    let userAmount

    if (
      input.status === TransactionStatus.PAID &&
      wallet?.amount !== undefined &&
      balance?.status === TransactionStatus.NEW
    ) {
      data.buyedAt = input.updatedAt
      userAmount =
        balance.type === 'DEPOSIT' ? wallet?.amount + +input.amount : wallet?.amount - +input.amount - coinDeposit.fee
    }

    const addressUser = await this._prisma.addressWallet.findFirst({
      where: { userId: input.userId, networkId: input.networkId }
    })
    if (balance.type === 'WITHDRAW' && !addressUser?.id) return this.withBadRequest('User address is not found')

    if (userAmount >= 0 && userAmount !== undefined) {
      await this._prisma.user.update({
        where: { id: user.id },
        data: {
          updatedAt: new Date(),
          Wallet: {
            update: {
              data: { amount: +userAmount },
              where: { id: wallet?.id }
            }
          }
        },
        select: { id: true }
      })
    } else {
      return this.withBadRequest('Amount is invalid')
    }

    if (input.amount === undefined) {
      return this.withBadRequest(['status: balance is pained'])
    }

    if (input.status === TransactionStatus.NEW) return this.withBadRequest(['status: balance is not yet purchased'])

    const newBalance = await this._prisma.balance.update({
      where: { id },
      data: {
        ...data,
        updatedAt: new Date(),
        histories: {
          create: {
            id: toId(),
            createdAt: input.updatedAt,
            userId: balance.userId ?? '',
            ownerUsersId: balance.ownerId ?? '',
            action: [{ content: JSON.stringify(data), createdAt: input.updatedAt }],
            type: balance.type
          }
        }
      },
      select: { updatedAt: true, id: true, userId: true, status: true }
    })

    if (balance.type === 'WITHDRAW' && !!addressUser?.id) {
      await this._prisma.addressWallet.update({
        where: { id: addressUser?.id },
        data: {
          addressWallet: input.addressWallet
        }
      })
    }
    return newBalance
  }
}
