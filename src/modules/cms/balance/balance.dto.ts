import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const BalanceSchema = z.object({
  id: z.string(),
  amount: z.string(),
  status: z.string(),
  symbolId: z.string(),
  networkId: z.string(),
  userId: z.string(),
  addressWallet: z.string()
})
export class UpdateBalanceDto extends createZodDto(BalanceSchema) {}
