import { BaseController } from '@/modules/base/controller'
import { IListQuery } from '@/modules/base/dto'
import { Body, Controller, Get, Param, Patch, Query, Req } from '@nestjs/common'
import { UpdateBalanceDto } from './balance.dto'
import { BalancesService } from './balance.service'

@Controller('cms/balances')
export class BalancesController extends BaseController {
  constructor(private readonly balancesService: BalancesService) {
    super()
  }

  @Get('find')
  findMany(@Query() query: IListQuery) {
    return this.balancesService.findMany(query)
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.balancesService.findOne(id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() body: UpdateBalanceDto, @Req() req: Request) {
    return this.balancesService.update(id, this.withUpdated(body, req))
  }
}
