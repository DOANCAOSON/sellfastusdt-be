import { HttpModule } from '@nestjs/axios'
import { Module } from '@nestjs/common'
import { TokensController } from './token.controller'
import { TokensService } from './token.service'

@Module({
  imports: [
    HttpModule.registerAsync({
      useFactory: () => ({
        timeout: 5000,
        maxRedirects: 5
      })
    })
  ],
  controllers: [TokensController],
  providers: [TokensService]
})
export class TokensModule {}
