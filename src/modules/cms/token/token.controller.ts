import { BaseController } from '@/modules/base/controller'
import { IListQuery } from '@/modules/base/dto'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Delete, Get, Param, Patch, Post, Query, Req, UseInterceptors } from '@nestjs/common'
import { TokenDto } from './token.dto'
import { TokensService } from './token.service'

@UseInterceptors(CacheInterceptor)
@Controller('cms/tokens')
export class TokensController extends BaseController {
  constructor(private readonly TokensService: TokensService) {
    super()
  }

  @Post()
  create(@Body() body: TokenDto) {
    return this.TokensService.create(body)
  }

  @Get('binance/find')
  findAdminRelation() {
    return this.TokensService.findAllBinance()
  }

  @Get('find')
  findMany(@Query() query: IListQuery, @Req() req: Request) {
    return this.TokensService.findAll(query)
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.TokensService.findOne(id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() body: TokenDto) {
    return this.TokensService.update(id, body)
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.TokensService.remove(id)
  }
}
