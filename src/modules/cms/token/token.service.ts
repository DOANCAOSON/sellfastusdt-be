import { BaseDto, IFilter, IListQuery } from '@/modules/base/dto'
import { removeImageFile, saveImageFile, toId, toSlug } from '@/modules/base/util'
import { HttpService } from '@nestjs/axios'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Injectable, NotFoundException, UseInterceptors } from '@nestjs/common'
import { readFileSync } from 'fs'
import { join } from 'path'
import { BaseService } from '../../base/service'
import { TokenDto } from './token.dto'

@UseInterceptors(CacheInterceptor)
@Injectable()
export class TokensService extends BaseService {
  private readonly filterable: Array<IFilter>
  constructor(private readonly httpService: HttpService) {
    super()
    const fileData = readFileSync(join(process.cwd(), '/prisma/schemas/token.json'), 'utf-8')
    const schema = JSON.parse(fileData.toString())
    this.filterable = schema['layouts'].filterable.flat()
  }

  async create(input: TokenDto) {
    const { images: imagesUpload, isActive, networks, ...data } = input
    if (isActive)
      return this.withBadRequest(['isActive: If you create a new one, you cannot set active for this owner'])
    if (imagesUpload) {
      const images = await saveImageFile(imagesUpload, 'product')
      data['images'] = images.join(',')
      data['imagesUpload'] = imagesUpload
    }

    return this._prisma.$transaction(
      async (tx) => {
        const slug = await this.generateSlug(data.tokenName)
        return tx.token.create({
          data: {
            ...data,
            id: toId(),
            slug,
            isActive,
            networkIds: networks
          },
          select: { id: true }
        })
      },
      { isolationLevel: 'ReadCommitted', maxWait: 10000, timeout: 10000 }
    )
  }

  findDrafts() {
    return this._prisma.token.findMany({
      select: {
        id: true,
        tokenName: true,
        whitepaper: true,
        createdAt: true,
        isActive: true
      }
    })
  }

  async findAll(query: IListQuery) {
    const { skip, take } = this.withLimitOffset(query)
    const where = this.withFilter(query, this.filterable)

    const [dataSource, total] = await Promise.all([
      this._prisma.token.findMany({
        select: {
          id: true,
          tokenName: true,
          whitepaper: true,
          slug: true,
          createdAt: true,
          binance: true,
          images: true,
          isActive: true,
          rateBuy: true,
          rateSell: true
        },
        where,
        orderBy: { createdAt: 'desc' },
        take,
        skip
      }),
      this._prisma.token.count({ where })
    ])
    const data = dataSource.map((d) => {
      d['key'] = d.id
      d['deletable'] = true
      return d
    })
    return this.withPagination(data, query, total)
  }

  async findOne(id: string) {
    const token = await this._prisma.token.findFirst({
      where: { id },
      select: {
        id: true,
        tokenName: true,
        binance: true,
        whitepaper: true,
        slug: true,
        createdAt: true,
        images: true,
        isActive: true,
        imagesUpload: true,
        rateBuy: true,
        rateSell: true,
        min: true,
        max: true,
        networkIds: true
      }
    })
    if (!token) throw new NotFoundException()
    const { imagesUpload } = token
    token['images'] = imagesUpload as any
    token['networks'] = token.networkIds
    return token
  }

  async findAllBinance() {
    const url = 'https://api3.binance.com/api/v3/ticker/price'
    try {
      const response = await this.httpService.axiosRef.get(url)
      const data = response.data
      const binances = data.map((d) => ({
        value: d.symbol.toLowerCase(),
        label: d.symbol,
        price: d.price
      }))
      return binances
    } catch (error) {
      throw 'An error occurred while fetching all data!'
    }
  }

  async update(id: string, input: Partial<TokenDto & BaseDto>) {
    const { images: imagesUpload, isActive, price = 0, rateBuy, rateSell, min, max, networks, ...data } = input

    const detail = await this._prisma.token.findFirst({
      where: { id },
      select: {
        imagesUpload: true
      }
    })
    if (!detail) {
      throw new NotFoundException()
    }
    if (isActive) {
      await this._prisma.token.updateMany({
        where: { isActive: true },
        data: {
          isActive: false
        }
      })
    }
    // if (imagesUpload) {
    //   const images = await saveImageFile(imagesUpload, 'token')
    //   data['images'] = images.join(',')
    //   data['imagesUpload'] = imagesUpload
    // }
    return this._prisma.$transaction(
      async (tx) => {
        return tx.token.update({
          where: { id },
          data: {
            ...data,
            rateBuy: Number(rateBuy),
            rateSell: Number(rateSell),
            min: Number(min),
            max: Number(max),
            isActive,
            networkIds: networks
          },
          select: { id: true }
        })
      },
      { isolationLevel: 'ReadCommitted', maxWait: 10000, timeout: 10000 }
    )
  }

  async remove(id: string) {
    const token = await this._prisma.token.findFirst({
      where: { id },
      select: { images: true }
    })
    if (!token) throw new NotFoundException()

    const output = await this._prisma.$transaction(
      async (tx) => {
        await Promise.all([tx.token.delete({ where: { id: id } })])

        return { id }
      },
      { isolationLevel: 'ReadCommitted', maxWait: 10000, timeout: 10000 }
    )

    setTimeout(() => {
      removeImageFile(token.images)
    }, 0)

    return output
  }

  private async generateSlug(name: string) {
    let slug = toSlug(name)

    const count = await this._prisma.token.count({ where: { slug } })
    if (count > 0) {
      const count = await this._prisma.token.count()
      slug = `${slug}-${count + 1}`
    }

    return slug
  }
}
