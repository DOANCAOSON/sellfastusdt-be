import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const TokenSchema = z.object({
  images: z.object({ thumbUrl: z.string(), name: z.string(), type: z.string() }).array().max(10).nullable().optional(),
  tokenName: z.string().max(100),
  price: z.number(),
  isActive: z.boolean(),
  whitepaper: z.string(),
  binance: z.string(),
  min: z.number(),
  max: z.number(),
  rateBuy: z.number(),
  rateSell: z.number(),
  networks: z.string().array()
})
export class TokenDto extends createZodDto(TokenSchema) {}
