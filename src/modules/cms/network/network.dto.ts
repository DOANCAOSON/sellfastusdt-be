import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const NetworkSchema = z.object({
  name: z.string().max(100),
  label: z.string(),
  value: z.string(),
  description: z.string().max(100).optional().nullable(),
  addressOwner: z.string(),
  isActive: z.boolean(),
  tokenBinance: z.string()
})
export class NetworkDto extends createZodDto(NetworkSchema) {}
