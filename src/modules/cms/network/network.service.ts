import { BaseDto, CurrentUser, IFilter, IListQuery } from '@/modules/base/dto'
import { toId, toSlug } from '@/modules/base/util'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Injectable, NotFoundException, UseInterceptors } from '@nestjs/common'
import { readFileSync } from 'fs'
import { join } from 'path'
import { BaseService } from '../../base/service'
import { NetworkDto } from './network.dto'

@UseInterceptors(CacheInterceptor)
@Injectable()
export class NetworksService extends BaseService {
  private readonly filterable: Array<IFilter>
  constructor() {
    super()
    const fileData = readFileSync(join(process.cwd(), '/prisma/schemas/network.json'), 'utf-8')
    const schema = JSON.parse(fileData.toString())
    this.filterable = schema['layouts'].filterable.flat()
  }

  async create(input: NetworkDto) {
    const { addressOwner, tokenBinance } = input

    const ownerUser = await this._prisma.ownerUsers.findFirst({
      where: { isActive: true }
    })

    const slug = await this.generateSlug(input.name)
    const token = await this._prisma.token.findFirst({ where: { binance: tokenBinance } })
    if (!token) return this.withBadRequest('Token is not found')
    const id = toId()
    const network = await this._prisma.network.create({
      data: {
        id,
        name: input.name,
        slug: slug,
        label: input.label,
        description: input.description,
        isActive: input.isActive,
        value: input.value
      },
      select: { id: true }
    })

    if (!!addressOwner)
      await this._prisma.addressWallet.create({
        data: {
          id: toId(),
          addressWallet: addressOwner,
          userId: ownerUser?.id ?? '',
          networkId: id
        },
        select: {
          id: true
        }
      })

    return network
  }

  async findAll(query: IListQuery, user: CurrentUser) {
    const { skip, take } = this.withLimitOffset(query)
    const where = this.withFilter(query, this.filterable)

    const [dataSource, total] = await Promise.all([
      this._prisma.network.findMany({
        select: {
          id: true,
          name: true,
          slug: true,
          isActive: true,
          createdAt: true,
          label: true,
          value: true,
          description: true
        },
        where,
        orderBy: { createdAt: 'desc' },
        take,
        skip
      }),
      this._prisma.network.count({ where })
    ])
    const isSuperAdmin = user.roles.includes('super-admin')

    const data = dataSource.map((d) => {
      d['key'] = d.id
      d['deletable'] = isSuperAdmin
      return d
    })
    return this.withPagination(data, query, total)
  }

  async findOne(id: string) {
    const network = await this._prisma.network.findFirst({
      where: { id },
      select: {
        name: true,
        slug: true,
        isActive: true,
        createdAt: true,
        label: true,
        description: true,
        updatedAt: true,
        id: true,
        value: true
      }
    })
    const address = await this._prisma.addressWallet.findFirst({
      where: { networkId: network?.id }
    })

    if (!network) throw new NotFoundException()
    network['addressOwner'] = address?.addressWallet
    return network
  }

  async update(id: string, input: Partial<NetworkDto & BaseDto>) {
    const ownerUser = await this._prisma.ownerUsers.findFirst({
      where: { isActive: true }
    })
    const address = await this._prisma.addressWallet.findFirst({
      where: { userId: ownerUser?.id, networkId: id }
    })

    const now = new Date()
    return this._prisma.$transaction(
      async (tx) => {
        if (!!input.addressOwner) {
          await tx.addressWallet.upsert({
            where: { id: address?.id ?? '' },
            create: {
              id: toId(),
              userId: ownerUser?.id ?? '',
              networkId: id,
              createdAt: now,
              updatedAt: now,
              addressWallet: input.addressOwner
            },
            update: {
              addressWallet: input.addressOwner
            }
          })
        }
        return tx.network.update({
          where: { id },
          data: {
            name: input.name,
            label: input.label,
            description: input.description,
            isActive: input.isActive,
            value: input.value
          },
          select: { id: true }
        })
      },
      { isolationLevel: 'ReadCommitted', maxWait: 10000, timeout: 10000 }
    )
  }

  async remove(id: string) {
    const network = await this._prisma.network.findFirst({
      where: { id }
    })
    if (!network) throw new NotFoundException()

    const output = await this._prisma.$transaction(
      async (tx) => {
        await Promise.all([tx.network.delete({ where: { id: id } })])

        return { id }
      },
      { isolationLevel: 'ReadCommitted', maxWait: 10000, timeout: 10000 }
    )

    return output
  }

  private async generateSlug(name: string) {
    let slug = toSlug(name)

    const count = await this._prisma.network.count({ where: { slug } })
    if (count > 0) {
      const count = await this._prisma.network.count()
      slug = `${slug}-${count + 1}`
    }

    return slug
  }
}
