import { BaseController } from '@/modules/base/controller'
import { IListQuery } from '@/modules/base/dto'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Delete, Get, Param, Patch, Post, Query, Req, UseInterceptors } from '@nestjs/common'
import { NetworkDto } from './network.dto'
import { NetworksService } from './network.service'

@UseInterceptors(CacheInterceptor)
@Controller('cms/networks')
export class NetworksController extends BaseController {
  constructor(private readonly networksService: NetworksService) {
    super()
  }

  @Post()
  create(@Body() body: NetworkDto, @Req() req: Request) {
    return this.networksService.create(this.withUpdated(body, req))
  }

  @Get('find')
  findMany(@Query() query: IListQuery, @Req() req: Request) {
    return this.networksService.findAll(query, req['user'])
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.networksService.findOne(id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() body: NetworkDto, @Req() req: Request) {
    return this.networksService.update(id, this.withUpdated(body, req))
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.networksService.remove(id)
  }
}
