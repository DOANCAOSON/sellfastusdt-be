import { Module } from '@nestjs/common'
import { NetworksController } from './network.controller'
import { NetworksService } from './network.service'

@Module({
  controllers: [NetworksController],
  providers: [NetworksService]
})
export class NetworksModule {}
