import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const StaticPageSchema = z.object({
  html: z.string().nullable().optional()
})

export class StaticPageDto extends createZodDto(StaticPageSchema) {}
