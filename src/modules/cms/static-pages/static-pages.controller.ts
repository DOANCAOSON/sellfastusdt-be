import { BaseController } from '@/modules/base/controller'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Get, Param, Post, Req, UsePipes } from '@nestjs/common'
import { StaticPageDto } from './static-pages.dto'
import { StaticPagesService } from './static-pages.service'

@UsePipes(CacheInterceptor)
@Controller('cms/static-pages')
export class StaticPagesController extends BaseController {
  constructor(private readonly service: StaticPagesService) {
    super()
  }

  @Get(':id')
  async getPage(@Param('id') id: string) {
    return this.service.getPage(id)
  }

  @Post(':id')
  async updatePage(@Param('id') id: string, @Body() body: StaticPageDto, @Req() req: Request) {
    return this.service.updatePage(id, this.withUpdated(body, req))
  }

  @Get('telegram')
  async getTelegram() {
    return this.service.getTelegram()
  }

  @Post('telegram')
  async updateTelegram(@Body() body: StaticPageDto) {
    return this.service.updateTelegram(body.html || '')
  }

  @Get('promotion')
  async getPromotion() {
    return this.service.getPromotion()
  }

  @Post('promotion')
  async updatePromotion(@Body() body: StaticPageDto) {
    return this.service.updatePromotion(body.html || '')
  }

  @Get('headers')
  async getHeaders() {
    return this.service.getHeaders()
  }

  @Post('headers')
  async updateHeaders(@Body() body: StaticPageDto) {
    return this.service.updateHeaders(body.html || '')
  }
}
