import { BaseDto } from '@/modules/base/dto'
import { BaseService } from '@/modules/base/service'
import { readFile, saveFile, toId } from '@/modules/base/util'
import { Injectable } from '@nestjs/common'
import { StaticPageDto } from './static-pages.dto'

@Injectable()
export class StaticPagesService extends BaseService {
  updateTelegram(content: string) {
    saveFile('telegram', 'telegram.txt', content)
    return { status: true }
  }

  updatePromotion(content: string) {
    saveFile('promotion', 'promotion.txt', content)
    return { status: true }
  }

  getTelegram() {
    return readFile('telegram', 'telegram.txt')
  }

  getPromotion() {
    return readFile('promotion', 'promotion.txt')
  }

  updateHeaders(content: string) {
    saveFile('headers', 'headers.txt', content)
    return { status: true }
  }

  getHeaders() {
    return readFile('headers', 'headers.txt')
  }

  getPage(slug: string) {
    return this._prisma.staticPages.findFirst({ where: { slug }, select: { html: true } })
  }

  updatePage(slug: string, data: StaticPageDto) {
    const { createdAt, createdById, updatedAt, updatedById } = data as BaseDto
    return this._prisma.staticPages.upsert({
      where: { slug },
      create: { id: toId(), slug, html: data.html, createdAt, createdById, updatedAt, updatedById },
      update: { html: data.html, updatedAt, updatedById },
      select: { updatedAt: true }
    })
  }
}
