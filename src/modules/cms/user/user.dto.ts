import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const CreateSchema = z.object({
  email: z.string().max(100),
  addressWallet: z.string()
})
export class CreateUserBankDto extends createZodDto(CreateSchema) {}
