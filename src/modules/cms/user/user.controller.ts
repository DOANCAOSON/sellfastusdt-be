import { BaseController } from '@/modules/base/controller'
import type { IListQuery } from '@/modules/base/dto'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Delete, Get, Param, Patch, Post, Query, Req, UseInterceptors } from '@nestjs/common'
import { CreateUserBankDto } from './user.dto'
import { UserService } from './user.service'

@UseInterceptors(CacheInterceptor)
@Controller('cms/users')
export class UserController extends BaseController {
  constructor(private readonly userService: UserService) {
    super()
  }

  @Get('find')
  findMany(@Query() query: IListQuery, @Req() req: Request) {
    return this.userService.findMany(query, req['user'])
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.userService.findOne(id)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() body: CreateUserBankDto, @Req() req: Request) {
    return this.userService.update(id, this.withUpdated(body, req))
  }

  @Post()
  patchAdmin(@Body() body: CreateUserBankDto) {
    return this.userService.createUserAdmin(body)
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.userService.remove(id)
  }
}
