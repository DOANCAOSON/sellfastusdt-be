import type { BaseDto, CurrentUser, IFilter, IListQuery } from '@/modules/base/dto'
import { toId } from '@/modules/base/util'
import { Injectable, NotFoundException } from '@nestjs/common'
import { readFileSync } from 'fs'
import { join } from 'path'
import { BaseService } from '../../base/service'
import { CreateUserBankDto } from './user.dto'

@Injectable()
export class UserService extends BaseService {
  private readonly filterable: Array<IFilter>
  constructor() {
    super()
    const fileData = readFileSync(join(process.cwd(), '/prisma/schemas/user.json'), 'utf-8')
    const schema = JSON.parse(fileData.toString())
    this.filterable = schema['layouts'].filterable.flat()
  }

  async findMany(query: IListQuery, user: CurrentUser) {
    const { skip, take } = this.withLimitOffset(query)
    const where = this.withFilter(query, this.filterable)

    const [dataSource, total] = await Promise.all([
      this._prisma.user.findMany({
        select: {
          id: true,
          email: true,
          createdAt: true
        },
        where,
        orderBy: { createdAt: 'desc' },
        take,
        skip
      }),
      this._prisma.user.count({ where })
    ])

    const isSuperAdmin = user.roles.includes('super-admin')

    for (const d of dataSource) {
      const transaction = await this._prisma.transaction.count({ where: { userId: d.id } })
      const balance = await this._prisma.balance.count({ where: { userId: d.id } })

      d['key'] = d.id
      d['marketCount'] = transaction
      d['walletCount'] = balance
      d['deletable'] = isSuperAdmin
    }

    return this.withPagination(dataSource, query, total)
  }

  async findOne(id: string) {
    const user = await this._prisma.user.findFirst({
      where: { id },
      select: {
        id: true,
        email: true,
        createdAt: true
      }
    })

    if (!user) {
      throw new NotFoundException()
    }

    const wallet = await this._prisma.wallet.findMany({
      where: { userId: user.id },
      select: {
        id: true,
        Token: true,
        binance: true,
        amount: true
      }
    })
    user['Wallet'] = wallet.map((w) => ({
      binance: w.binance.replace('usdt', '').toUpperCase(),
      images: w.Token.images,
      amount: w.amount
    }))
    return user
  }

  async update(id: string, input: Partial<CreateUserBankDto & BaseDto>) {
    const { addressWallet, email, ...data } = input

    const user = await this._prisma.user.findFirst({
      where: { id },
      select: {
        id: true
      }
    })
    if (!user) {
      throw new NotFoundException()
    }

    return this._prisma.$transaction(
      async (tx) => {
        await Promise.all([
          tx.user.update({
            where: { id },
            data: {
              email
            }
          })
        ])
        return { id }
      },
      { isolationLevel: 'ReadCommitted', maxWait: 10000, timeout: 10000 }
    )
  }

  async createUserAdmin(body: CreateUserBankDto) {
    const { email } = body

    const user = await this._prisma.user.create({
      data: { email, id: toId() },
      select: {
        id: true,
        createdAt: true
      }
    })
    return { user }
  }

  async remove(id: string) {
    const user = await this._prisma.user.delete({
      where: { id },
      select: { id: true }
    })
    return user
  }
}
