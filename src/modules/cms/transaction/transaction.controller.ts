import { BaseController } from '@/modules/base/controller'
import { IListQuery } from '@/modules/base/dto'
import { Body, Controller, Get, Param, Patch, Query, Req } from '@nestjs/common'
import { UpdateTransactionDto } from './transaction.dto'
import { TransactionService } from './transaction.service'

@Controller('cms/transactions')
export class TransactionController extends BaseController {
  constructor(private readonly transactionService: TransactionService) {
    super()
  }

  @Get('find')
  findMany(@Query() query: IListQuery) {
    return this.transactionService.findMany(query)
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.transactionService.findOne(id)
  }

  @Get('export')
  exportData(@Query() query: IListQuery) {
    return this.transactionService.export(query)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() body: UpdateTransactionDto, @Req() req: Request) {
    return this.transactionService.update(id, this.withUpdated(body, req))
  }
}
