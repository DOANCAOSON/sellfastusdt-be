import { Module } from '@nestjs/common'
import { TransactionController } from './transaction.controller'
import { TransactionService } from './transaction.service'
import { ConfigService } from '@nestjs/config'

@Module({
  controllers: [TransactionController],
  providers: [TransactionService, ConfigService]
})
export class TransactionModule {}
