import { BaseDto, IFilter, IListDto, IListQuery } from '@/modules/base/dto'
import { BaseService } from '@/modules/base/service'
import { toId } from '@/modules/base/util'
import { Injectable, NotFoundException } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { JwtService } from '@nestjs/jwt'
import { Workbook } from 'exceljs'
import { readFileSync } from 'fs'
import { join } from 'path'
import { TransactionStatus, UpdateTransactionDto } from './transaction.dto'

const TRANSACTION_HEADERS = [
  'CODE',
  'AMOUNT',
  'TYPE',
  'PRICE',
  'STATUS',
  'IP',
  'ADDRESS PAIN',
  'ADDRESS RECEIVED',
  'CREATED AT'
]

@Injectable()
export class TransactionService extends BaseService {
  private readonly filterable: Array<IFilter>

  constructor(private readonly configService: ConfigService, private jwtService: JwtService) {
    super()
    const fileData = readFileSync(join(process.cwd(), '/prisma/schemas/transaction.json'), 'utf-8')
    const schema = JSON.parse(fileData.toString())
    this.filterable = schema['layouts'].filterable.flat()
  }

  async findMany(query: IListQuery & { source?: string }): Promise<IListDto> {
    const { skip, take } = this.withLimitOffset(query)
    const where = this.withFilter(query, this.filterable)
    const [dataSource, total] = await Promise.all([
      this._prisma.transaction.findMany({
        select: {
          id: true,
          code: true,
          createdAt: true,
          ethereumAmount: true,
          status: true,
          ip: true,
          coinAmount: true,
          userId: true,
          ownerUsersId: true,
          transactionToken: true,
          type: true,
          binance: true
        },
        where,
        orderBy: { createdAt: 'desc' },
        take,
        skip
      }),
      this._prisma.transaction.count({ where })
    ])

    const currentTime = Math.floor(Date.now() / 1000)

    const onDecodedToken = async (d: any) => {
      const detail = await this.verifyTransactionToken(d.transactionToken, d.id)

      const decodedToken = await this.jwtService
        .verifyAsync(d.transactionToken, {
          secret: this.configService.get('SECRET_TOKEN')
        })
        .catch(() => {
          if (detail && detail['status'] === TransactionStatus.NEW) {
            detail['status'] = 'CANCELLED'
          }
        })
      return decodedToken
    }

    const data: any = []
    for (const d of dataSource) {
      if (d.status === 'NEW' && d.transactionToken) {
        const decodedToken = await onDecodedToken(d)
        if (decodedToken && decodedToken.exp - currentTime > 0 && d.status === 'NEW') {
          d['expired'] = decodedToken.exp - currentTime
        }
      }
      const symbol = await this._prisma.token.findFirst({
        where: { binance: d.binance ?? '' },
        select: { tokenName: true, images: true }
      })
      d['images'] = symbol?.images
      d['tokenName'] = symbol?.tokenName
      d['key'] = d.id
      data.push(d)
    }

    return this.withPagination(data, query, total)
  }

  private async verifyTransactionToken(token: string, id: string) {
    return await this.jwtService
      .verifyAsync(token, { secret: this.configService.get('SECRET_TOKEN') })
      .then(async () => {
        return await this._prisma.transaction.findFirst({
          select: {
            id: true,
            code: true,
            createdAt: true,
            type: true,
            ethereumAmount: true,
            status: true,
            expired: true,
            ip: true,
            transactionToken: true,
            User: {
              select: {
                email: true,
                id: true
              }
            },
            OwnerUsers: {
              select: {
                email: true,
                id: true
              }
            }
          },
          where: { id }
        })
      })
      .catch(async () => {
        const detail = await this._prisma.transaction.update({
          select: {
            id: true,
            code: true,
            createdAt: true,
            type: true,
            ethereumAmount: true,
            status: true,
            expired: true,
            ip: true,
            transactionToken: true,
            User: {
              select: {
                email: true,
                id: true
              }
            },
            OwnerUsers: {
              select: {
                email: true,
                id: true
              }
            }
          },
          where: { id },
          data: {
            status: 'CANCELLED'
          }
        })
        detail['status'] = 'CANCELLED'
        return detail
      })
  }

  async findOne(id: string) {
    const detail = await this._prisma.transaction.findFirst({
      select: {
        id: true,
        code: true,
        createdAt: true,
        type: true,
        ethereumAmount: true,
        coinAmount: true,
        status: true,
        expired: true,
        binance: true,
        ip: true,
        User: {
          select: {
            email: true,
            id: true
          }
        },
        OwnerUsers: {
          select: {
            email: true,
            id: true
          }
        },
        userId: true
      },
      where: { id }
    })
    if (!detail) throw new NotFoundException()

    detail['createdBy'] = detail?.User?.email
    const userOwner = detail?.OwnerUsers ? { id: detail?.OwnerUsers.id, label: 'User Pain', path: '/owner-users' } : {}
    const userUser = detail?.User ? { id: detail?.User.id, label: 'User Received', path: '/users' } : {}
    const Token = await this._prisma.token.findFirst({
      where: { binance: detail.binance ?? '' },
      select: { tokenName: true, images: true }
    })
    detail['User'] = { ...detail?.User, id: userUser } as any
    detail['Token'] = Token
    detail['OwnerUsers'] = { ...detail?.OwnerUsers, id: userOwner } as any

    return detail
  }

  async export(query: IListQuery & { source?: string }) {
    const where = this.withFilter(query, this.filterable)
    if (query.source) {
      if (query.source === 'ORIGINAL') {
        where.AND?.push({ ladiPage: null })
      } else if (query.source === 'LADIPAGE') {
        where.AND?.push({ ladiPage: { not: null } })
      }
    }
    const dataSource = await this._prisma.transaction.findMany({
      select: {
        id: true,
        code: true,
        createdAt: true,
        type: true,
        ethereumAmount: true,
        status: true,
        expired: true,
        ip: true,
        User: {
          select: {
            email: true,
            id: true
          }
        },
        OwnerUsers: {
          select: {
            email: true,
            id: true
          }
        }
      },
      where,
      orderBy: { createdAt: 'desc' }
    })

    const data: any[][] = [TRANSACTION_HEADERS]
    const wb = new Workbook()
    const ws = wb.addWorksheet('Data')

    dataSource.forEach((d) => {
      const rows: any[] = []
      rows.push(d.code) // CODE
      rows.push(d.ethereumAmount) // AMOUNT
      rows.push(d.type) // TYPE
      rows.push(d.status) // STATUS
      rows.push(d.expired) // expired
      rows.push(d.ip) // ip
      rows.push(d.createdAt)
      data.push(rows)
    })

    ws.addRows(data)

    return wb.xlsx.writeBuffer()
  }

  async update(id: string, input: UpdateTransactionDto & BaseDto) {
    const transaction = await this._prisma.transaction.findFirst({
      where: { id },
      select: {
        status: true,
        id: true,
        type: true,
        userId: true,
        transactionToken: true,
        ownerUsersId: true,
        ethereumAmount: true,
        coinAmount: true,
        binance: true
      }
    })

    const coinActive = await this._prisma.token.findFirst({
      where: { isActive: true },
      select: {
        id: true
      }
    })

    let [user, coinDeposit, wallet, walletUsdt] = await Promise.all([
      this._prisma.user.findFirst({
        where: { id: transaction?.userId },
        select: {
          id: true
        }
      }),
      this._prisma.token.findFirst({
        where: { binance: transaction?.binance ?? '' },
        select: {
          id: true,
          images: true
        }
      }),
      this._prisma.wallet.findFirst({
        where: { userId: transaction?.userId, binance: transaction?.binance as any }
      }),
      this._prisma.wallet.findFirst({
        where: { userId: transaction?.userId, binance: 'tusdusdt', tokenId: coinActive?.id }
      })
    ])

    if (!transaction) return this.withBadRequest('transaction is cancelled')
    await this.jwtService
      .verifyAsync(transaction.transactionToken ?? '', { secret: this.configService.get('SECRET_TOKEN') })
      .catch(async () => {
        return this.withBadRequest(['status: transaction is expired'])
      })

    const data: any = {
      status: input.status as any
    }

    if (
      (TransactionStatus.CONFIRMED === transaction?.status && data?.status === TransactionStatus.PAID) ||
      (TransactionStatus.CANCELLED === transaction?.status && data?.status === TransactionStatus.PAID)
    ) {
      return this.withBadRequest(['status: transaction is pained'])
    }

    if (TransactionStatus.NEW === input.status || transaction?.status === TransactionStatus.PAID)
      return this.withBadRequest(['status: transaction is pained'])

    if (TransactionStatus.CONFIRMED === input.status) {
      data.status = input.status
      data.confirmedAt = input.updatedAt
    }

    if (TransactionStatus.PAID === input.status && transaction.status === TransactionStatus.NEW) {
      if (!coinDeposit) {
        coinDeposit = coinActive as any
      }
      let userAmount
      if (transaction.type === 'BUY') {
        userAmount = (wallet?.amount as any) + transaction.coinAmount
        const coinAmount = (walletUsdt?.amount as any) - transaction.ethereumAmount

        if (userAmount >= 0 && coinAmount >= 0) {
          await this._prisma.wallet.update({
            where: { id: walletUsdt?.id },
            data: {
              amount: userAmount
            }
          })
        } else {
          return this.withBadRequest('Amount is invalid')
        }
      } else {
        userAmount = (wallet?.amount as any) - transaction.ethereumAmount
        const coinAmount = (wallet?.amount as any) + transaction.coinAmount
        if (userAmount >= 0 && coinAmount >= 0) {
          await this._prisma.wallet.update({
            where: { id: walletUsdt?.id },
            data: {
              amount: coinAmount
            }
          })
        } else {
          return this.withBadRequest('Amount is invalid')
        }
      }

      if (userAmount >= 0 && userAmount !== undefined) {
        await this._prisma.user.update({
          where: { id: user?.id },
          data: {
            updatedAt: new Date(),
            Wallet: {
              update: {
                where: { id: wallet?.id },
                data: {
                  amount: +userAmount
                }
              }
            }
          },
          select: { id: true }
        })
      } else {
        return this.withBadRequest(['Amount: Amount is invalid'])
      }
    }

    if (TransactionStatus.CANCELLED === input.status) {
      data.cancelAt = input.updatedAt
    }

    if (transaction.status === TransactionStatus.PAID) {
      return this.withBadRequest(['status: transaction is pained'])
    }
    const newTransaction = await this._prisma.transaction.update({
      where: { id },
      data: {
        ...data,
        updatedAt: new Date(),
        histories: {
          create: {
            id: toId(),
            createdAt: input['updatedAt'],
            userId: transaction.userId ?? '',
            action: [{ content: JSON.stringify(data), createdAt: input['updatedAt'] }],
            type: transaction.type
          }
        }
      },
      select: {
        updatedAt: true,
        id: true,
        userId: true,
        status: true,
        type: true,
        code: true
      }
    })

    return { ...newTransaction, image: coinDeposit?.images }
  }
}
