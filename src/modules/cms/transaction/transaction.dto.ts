import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

export enum TransactionStatus {
  CONFIRMED = 'CONFIRMED',
  CANCELLED = 'CANCELLED',
  NEW = 'NEW',
  PAID = 'PAID'
}

export enum BuyStatus {
  PAID = 'PAID',
  PENDING = 'PENDING',
  VOID = 'VOID',
  NA = 'NA'
}

export enum SellStatus {
  PAID = 'PAID',
  PENDING = 'PENDING',
  VOID = 'VOID',
  NA = 'NA'
}

const TransactionSchema = z.object({
  buyerId: z.string().max(30),
  ownerUsersId: z.string().max(30),
  ethereumAmount: z.number().max(100),
  price: z.number().max(100),
  status: z.string().max(100).optional().nullable(),
  published: z.boolean()
})
export class TransactionDto extends createZodDto(TransactionSchema) {}

const UpdateTransactionSchema = z.object({
  status: z.string().nullable().optional(),
  id: z.string(),
  buyStatus: z.string().nullable().optional(),
  sellStatus: z.string().nullable().optional(),
  note: z.string().max(512).nullable().optional(),
  userId: z.string()
})
export class UpdateTransactionDto extends createZodDto(UpdateTransactionSchema) {}
