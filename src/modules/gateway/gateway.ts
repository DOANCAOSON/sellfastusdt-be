import { HttpService } from '@nestjs/axios'
import { Inject, OnModuleInit, forwardRef } from '@nestjs/common'
import { MessageBody, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets'
import { Server, Socket } from 'socket.io'
import { DefaultEventsMap } from 'socket.io/dist/typed-events'
import * as WebSocket from 'ws'
import { BaseController } from '../base/controller'
import { UpdateBalanceDto } from '../cms/balance/balance.dto'
import { TokensService } from '../cms/token/token.service'
import { UpdateTransactionDto } from '../cms/transaction/transaction.dto'
import { BalancesService } from '../landing/balance/balance.service'
import { NotifyDto } from '../landing/notify/notify.dto'
import { NotifyService } from '../landing/notify/notify.service'
import { TransactionService } from '../landing/transaction/transaction.service'

const urlOrder = 'https://api.binance.com/api/v3/depth'
@WebSocketGateway(0, { cors: true, namespace: 'socket' })
export class GetWayService extends BaseController implements OnModuleInit {
  private users: { id: string; socketIds: string[] }[] = []
  private socket: Socket<any, any, DefaultEventsMap, any>
  private binanceSocket: Record<string, WebSocket> = {}
  private binances: any = []
  private symbol = 'tusdusdt'

  constructor(
    @Inject(forwardRef(() => TransactionService))
    private transactionService: TransactionService,
    private notifyService: NotifyService,
    private balancesService: BalancesService,
    private readonly httpService: HttpService,
    private tokenService: TokensService
  ) {
    super()
    WebSocket.defaultMaxListeners = 20
  }

  @WebSocketServer()
  server: Server<any>

  async onModuleInit() {
    this.server.on('connection', (s) => {
      this.socket = s
    })
    const data = await this.getTokensData()
    this.binances = data || []
  }

  @SubscribeMessage('fetchSymbols')
  async onGetDataBinanceByDate(
    @MessageBody()
    {
      symbol,
      start,
      stop,
      time,
      clientId
    }: {
      symbol: string
      start: string
      stop: string
      time: string
      clientId: string
    }
  ) {
    this.symbol = symbol
    const data = await this.getHistoricalData(start, stop, time)

    this.server.to(clientId).emit(`coin_${this.symbol}`, { data, symbol: this.symbol })
  }

  @SubscribeMessage('orderBookData')
  async onOrderBookData(
    @MessageBody()
    { symbol }: { symbol: string }
  ) {
    const params = {
      symbol: symbol.toUpperCase(),
      limit: 50
    }

    try {
      const response = await this.httpService.axiosRef.get(urlOrder, { params })
      const orderBookData = response.data

      let totalQuantity = 0
      let totalBTC = 0
      let totalUSDT = 0

      orderBookData.bids.forEach((bid) => {
        const price = parseFloat(bid[0])
        const quantity = parseFloat(bid[1])
        totalQuantity += quantity
        totalBTC += price * quantity
      })

      orderBookData.asks.forEach((ask) => {
        const price = parseFloat(ask[0])
        const quantity = parseFloat(ask[1])
        totalQuantity += quantity
        totalUSDT += price * quantity
      })

      const wap = (totalBTC + totalUSDT) / totalQuantity
      this.server.emit(`card_order_book_${symbol.toLowerCase()}`, {
        ...orderBookData,
        wap,
        totalBTC,
        totalUSDT,
        symbol
      })
    } catch (error) {
      console.error('Error fetching or processing data:', error)
    }
  }

  @SubscribeMessage('updateSymbol')
  async onUpdateSymbol(@MessageBody() { symbol }: { symbol: string }) {
    const url = `wss://stream.binance.com:9443/ws`

    if (this.binanceSocket?.[symbol]) {
      this.binanceSocket[symbol].close()
    }
    this.binanceSocket[symbol] = new WebSocket(url)

    this.binanceSocket[symbol].onopen = () => {
      this.binanceSocket[symbol].send(
        JSON.stringify({
          method: 'SUBSCRIBE',
          params: [
            `${symbol}@avgPrice`,
            `${symbol}@ticker_1h`,
            `${symbol}@ticker_4h`,
            `${symbol}@ticker_1d`,
            `${symbol}@kline_1w`
          ],
          id: symbol
        })
      )
    }

    this.binanceSocket[symbol].on('error', (error) => {
      console.error('WebSocket error:', error)
    })
    const binance = this.binances.find((item) => item.binance === symbol.toLowerCase())
    this.binanceSocket[symbol].on('message', (event) => {
      const decoder = new TextDecoder('utf-8')
      const data = decoder.decode(event)

      try {
        const jsonData = JSON.parse(data)

        switch (jsonData.e) {
          case '1hTicker':
            return this.server.emit(symbol, { value: +jsonData.l, key: '1hTicker' })
          case '4hTicker':
            return this.server.emit(symbol, { value: +jsonData.l, key: '4hTicker' })
          case '1dTicker':
            this.server.emit(`card_row_${symbol.toLowerCase()}`, {
              ...binance,
              low: +jsonData.l,
              volume: +jsonData.c * binance.rateBuy,
              marker: +jsonData.q,
              high: +jsonData.h,
              change: +jsonData.P
            })
            return this.server.emit(symbol, {
              value: +jsonData.l,
              key: '1dTicker',
              volume: +jsonData.v,
              marker: +jsonData.q
            })
          case 'kline':
            return this.server.emit(symbol, { value: +jsonData.k.l, key: 'kline', time: +jsonData.E })
          case 'avgPrice':
            return this.server.emit(symbol, { value: +jsonData.w, key: 'avgPrice' })
        }
      } catch (error) {
        console.error('Lỗi khi parse dữ liệu JSON:', error)
      }
    })
  }

  async getPriceCoin(symbol: string = this.symbol, clientId?: string): Promise<any> {
    return new Promise((resolve, reject) => {
      if (!symbol || !this.binanceSocket[symbol]) {
        return
      }
      const handleBinanceMessage = async (event) => {
        const decoder = new TextDecoder('utf-8')
        const data = decoder.decode(event)
        const jsonData = JSON.parse(data)
        if (jsonData.e === '1dTicker') {
          const data = (await this.getTokensData()) as any
          const binance = data.find((item) => item.binance === symbol)
          if (!binance) {
            return
          }

          const priceBuy = +jsonData.c * binance.rateBuy
          const priceSell = +jsonData.c * binance.rateSell
          const coin = { priceBuy, priceSell, images: binance.images, tokenName: binance.tokenName }

          if (clientId) {
            this.server.to(clientId).emit(`price_coin_${symbol}`, { coin, symbol: symbol })
          }

          resolve(coin)
        }
      }

      this.binanceSocket[symbol].on('message', handleBinanceMessage)
    })
  }

  @SubscribeMessage('fetchPriceCoin')
  async onGetDataPriceCoin(
    @MessageBody()
    { symbol, clientId }: { symbol: string; clientId: string }
  ) {
    this.getPriceCoin(symbol, clientId)
  }

  async _makeRequest(endpoint: string, params: object) {
    try {
      const url = 'https://api.binance.com' + endpoint
      const response = await this.httpService.axiosRef.get(url, { params })
      return response.data
    } catch (error) {
      console.error(`Error making request: ${error}`)
      return null
    }
  }

  async fetchHistoricalData(interval = '15m', startTime = null, endTime = null, limit = 500) {
    const params = { symbol: this.symbol, interval, limit, startTime, endTime }
    const endpoint = '/api/v3/klines'
    const data = await this._makeRequest(endpoint, params)

    if (data) {
      return data.map((candle) => ({
        time: candle[0] / 1000,
        open: parseFloat(candle[1]),
        high: parseFloat(candle[2]),
        low: parseFloat(candle[3]),
        close: parseFloat(candle[4]),
        value: parseFloat(candle[7])
      }))
    } else {
      return null
    }
  }

  async getHistoricalData(startTime, endTime, time) {
    let collection: any = []
    while (startTime < endTime) {
      const data = await this.fetchHistoricalData(time, startTime, endTime, 500)
      if (data && data.length > 0) {
        startTime = data[data.length - 1].openTime + 60000
        collection.push(...data)
      } else {
        break
      }
      await new Promise((resolve) => setTimeout(resolve, 1100))
    }

    return collection
  }

  async getTokensData() {
    const binances = await this.tokenService.findAll({} as any)
    return binances.dataSource
  }

  @SubscribeMessage('join')
  async onJoin(@MessageBody() user: { id: string }) {
    let existingUser = this.users.find((u) => u.id === user.id)
    if (!existingUser) {
      existingUser = { id: user.id, socketIds: [this.socket.id] }
      this.users.push(existingUser)
    } else {
      existingUser.socketIds.push(this.socket.id)
    }
  }

  @SubscribeMessage('notifies')
  async onNewMessage(@MessageBody() body: NotifyDto) {
    const notify = await this.notifyService.create(body)
    const user = this.users.find((user) => user.id === body.userId)

    user?.socketIds.forEach((s) => {
      this.server.to(s).emit('notify', {
        msg: 'notify',
        content: notify
      })
    })
  }

  @SubscribeMessage('transactions')
  async onNewNotify(@MessageBody() body: UpdateTransactionDto) {
    const transaction = (await this.transactionService.findOne(body.id)) as any
    const user = this.users.find((user) => user.id === body.userId)
    const message = {
      body: `Transaction ${transaction.type} - ${transaction.status?.message}`,
      icon: 'https://sellfastusdt.com/assets/images/logo.png',
      url: `https://sellfastusdt.com/transaction/${transaction.type?.toLowerCase()}?transactionID=${transaction.code}`,
      title: 'Sellfastusdt'
    }
    user?.socketIds.forEach((s) => {
      this.server.to(s).emit('transaction', {
        msg: 'transaction',
        content: transaction,
        message
      })
    })
  }

  @SubscribeMessage('balances')
  async onBalanceRequest(@MessageBody() body: UpdateBalanceDto) {
    const balance = (await this.balancesService.findOne(body.id)) as any
    const user = this.users.find((user) => user.id === body.userId)
    const message = {
      body: `Transaction ${balance.code} - ${balance.status?.message}`,
      icon: 'https://sellfastusdt.com/assets/images/logo.png',
      url: `https://sellfastusdt.com/crypto${balance.code}`,
      title: 'Sellfastusdt'
    }
    user?.socketIds.forEach((s) => {
      this.server.to(`${s}`).emit('balance', {
        msg: 'balance',
        content: balance,
        message
      })
    })
  }

  async handleDisconnect(client: Socket) {
    this.users.forEach((user) => {
      const index = user.socketIds.indexOf(client.id)
      if (index !== -1) {
        user.socketIds.splice(index, 1)
      }
    })
    this.users = this.users.filter((user) => user.socketIds.length > 0)
  }
}
