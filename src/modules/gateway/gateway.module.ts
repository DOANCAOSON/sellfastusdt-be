import { HttpModule } from '@nestjs/axios'
import { Module } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { APP_GUARD } from '@nestjs/core'
import { TokensService } from '../cms/token/token.service'
import { CMSAuthGuard } from '../guards/cms.guard'
import { BalancesService } from '../landing/balance/balance.service'
import { NotifyService } from '../landing/notify/notify.service'
import { TransactionService } from '../landing/transaction/transaction.service'
import { GetWayService } from './gateway'

@Module({
  imports: [
    HttpModule.registerAsync({
      useFactory: () => ({
        timeout: 5000,
        maxRedirects: 5
      })
    })
  ],
  providers: [
    GetWayService,
    TransactionService,
    NotifyService,
    BalancesService,
    TokensService,
    ConfigService,
    { provide: APP_GUARD, useClass: CMSAuthGuard }
  ]
})
export class GatewayModule {}
