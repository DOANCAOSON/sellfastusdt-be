import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const TokenSchema = z.object({
  amount: z.number().max(100),
  transactionType: z.string(),
  model: z.string(),
  price: z.number().max(100)
})
export class HomeDto extends createZodDto(TokenSchema) {}
