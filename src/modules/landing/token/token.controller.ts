import { Public } from '@/modules/guards/public.guard'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Controller, Get, Param, UseInterceptors } from '@nestjs/common'
import { TokenService } from './token.service'

@UseInterceptors(CacheInterceptor)
@Controller('landing/token')
export class TokenController {
  constructor(private readonly tokenService: TokenService) {}

  @Public()
  @Get(':slug/price')
  findOne(@Param('slug') slug: string) {
    return this.tokenService.findOne(slug)
  }
}
