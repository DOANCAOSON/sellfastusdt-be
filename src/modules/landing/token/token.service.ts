import { BaseService } from '@/modules/base/service'
import { HttpService } from '@nestjs/axios'
import { Injectable } from '@nestjs/common'

@Injectable()
export class TokenService extends BaseService {
  constructor(private readonly httpService: HttpService) {
    super()
  }

  async findOne(slug: string): Promise<any> {
    const url = 'https://api.binance.com/api/v3/avgPrice?symbol=' + slug.toUpperCase()
    try {
      const response = await this.httpService.axiosRef.get(url)
      return response.data
    } catch (error) {
      throw 'An error occurred while fetching all data!'
    }
  }
}
