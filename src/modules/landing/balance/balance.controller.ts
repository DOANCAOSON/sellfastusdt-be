import { BaseController } from '@/modules/base/controller'
import { IListQuery } from '@/modules/base/dto'
import { Body, Controller, Get, Param, Post, Query, Req } from '@nestjs/common'
import { balanceDto } from './balance.dto'
import { BalancesService } from './balance.service'

@Controller('landing/balance')
export class BalancesController extends BaseController {
  constructor(private readonly balancesService: BalancesService) {
    super()
  }

  @Get('/find/:userId')
  findDrafts(@Param('userId') userId: string, @Query() query: IListQuery) {
    return this.balancesService.findMany(userId, query)
  }

  @Post()
  create(@Body() body: balanceDto, @Req() req: Request) {
    return this.balancesService.create(this.withUpdated(body, req))
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.balancesService.findOne(id)
  }
}
