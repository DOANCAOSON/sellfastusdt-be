import { IListQuery } from '@/modules/base/dto'
import { toId } from '@/modules/base/util'
import { Injectable } from '@nestjs/common'
import { BaseService } from '../../base/service'
import { balanceDto } from './balance.dto'

@Injectable()
export class BalancesService extends BaseService {
  async create(input: balanceDto) {
    const { type, userId, price, userAddress, ...data } = input
    const balanceId = toId()
    const token = await this._prisma.token.findFirst({
      where: { id: input.symbolId }
    })

    const addressExits = await this._prisma.addressWallet.findFirst({
      where: { addressWallet: userAddress, networkId: input.netWorkId, userId },
      select: { id: true }
    })

    let existingWallet = await this._prisma.wallet.findFirst({
      where: { userId: userId, binance: token?.binance }
    })

    let addressWallet

    if (type === 'WITHDRAW' && !!userAddress) {
      addressWallet = await this._prisma.addressWallet.upsert({
        where: { id: addressExits?.id ?? '' },
        create: {
          id: toId(),
          userId,
          createdAt: new Date(),
          updatedAt: new Date(),
          addressWallet: userAddress,
          networkId: input.netWorkId
        },
        update: { addressWallet: userAddress },
        select: { id: true }
      })
    }

    if (!existingWallet) {
      const wallet = await this._prisma.wallet.create({
        data: {
          id: toId(),
          address: '',
          amount: data.amount,
          binance: token?.binance as any,
          createdAt: new Date(),
          updatedAt: new Date(),
          isActive: true,
          userId: userId,
          tokenId: token?.id ?? ''
        }
      })
      existingWallet = wallet
    }

    const now = new Date()

    const [balance, _] = await Promise.all([
      await this._prisma.balance.create({
        select: { id: true, code: true },
        data: {
          ...data,
          id: balanceId,
          type,
          status: 'NEW',
          userId,
          isActive: false,
          code: this.generateCode(),
          addressClientId: addressWallet?.id ?? ''
        }
      }),
      await this._prisma.user.update({
        where: { id: userId },
        data: { Wallet: { connect: { id: existingWallet?.id } } }
      })
    ])

    const notification = await this._prisma.notification.create({
      data: {
        id: toId(),
        userId,
        url: `https://sellfastusdt.com/crypto/${balance.id}`,
        content: `You have successfully created a deposit through the bank with the code ${balance.code}`,
        image: token?.images,
        isRead: false,
        createdAt: now,
        text: 'Transaction successfully created!',
        updatedAt: now
      }
    })
    return { notification, balance }
  }

  private generateCode() {
    return 'CB-' + Math.floor(1000000000 + Math.random() * 9000000000)
  }

  async findOne(id: string) {
    const balance = await this._prisma.balance.findFirst({
      where: { id },
      select: {
        histories: true,
        amount: true,
        addressClientId: true,
        addressOwnerId: true,
        buyedAt: true,
        createdById: true,
        id: true,
        netWorkId: true,
        ownerId: true,
        code: true,
        status: true,
        symbolId: true,
        type: true,
        userId: true,
        updatedAt: true
      }
    })
    if (!balance) return this.withBadRequest('Balance deposit not found!')
    const [user, token, address] = await Promise.all([
      this.getEntity('user', balance.userId),
      this.getEntity('token', balance.symbolId ?? ''),
      this._prisma.addressWallet.findFirst({
        where: { id: balance.addressClientId ?? '' },
        select: { addressWallet: true }
      })
    ])
    let detail = balance
    detail['createdBy'] = user?.email
    detail['token'] = { img: token?.images, name: token?.tokenName, price: token?.price, binance: token?.binance }
    detail['methodPayment'] = token.binance
    detail['ex'] = this.generateStatus(balance.status) as any
    detail['ethereumAmount'] = balance.amount
    detail['userAddress'] = address?.addressWallet
    return { ...detail }
  }

  private generateStatus(status: string) {
    switch (status) {
      case 'NEW':
        return { type: status, message: 'Waiting to receive money' }
      case 'PAID':
        return { type: status, message: 'Received money' }
      case 'CONFIRMED':
        return { type: status, message: 'Accomplished' }
      case 'CANCELLED':
        return { type: status, message: 'Cancelled' }
    }
  }

  async findMany(userId: string, query: IListQuery) {
    const { skip, take } = this.withLimitOffset({ page: query.page, pageSize: query.pageSize })
    const { page = 1, pageSize = 10 } = query

    const [data, totalRecords] = await Promise.all([
      this._prisma.balance.findMany({
        select: {
          id: true,
          addressClientId: true,
          addressOwnerId: true,
          amount: true,
          netWorkId: true,
          status: true,
          symbolId: true,
          type: true,
          ownerId: true
        },
        where: { userId },
        skip,
        take
      }),
      this._prisma.balance.count({
        where: { userId }
      })
    ])
    const formattedData = await Promise.all(
      data.map(async (d) => {
        const [ownerUser, network, token, addressClient, addressOwner] = await Promise.all([
          this.getEntity('ownerUsers', d.ownerId ?? ''),
          this.getEntity('network', d.netWorkId ?? ''),
          this.getEntity('token', d.symbolId ?? ''),
          this.getEntity('addressWallet', d.addressClientId ?? ''),
          this.getEntity('addressWallet', d.addressOwnerId ?? '')
        ])
        return {
          ...d,
          key: d.id,
          ownerUser,
          network,
          token,
          addressClient,
          addressOwner
        }
      })
    )
    return { data: formattedData, pagination: { page, pageSize, totalRecords } }
  }

  private async getEntity(entity: string, entityId: string) {
    return entityId ? await this._prisma[entity].findFirst({ where: { id: entityId } }) : null
  }
}
