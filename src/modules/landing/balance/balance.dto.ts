import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const BalanceSchema = z.object({
  type: z.string(),
  amount: z.number(),
  symbolId: z.string(),
  userId: z.string(),
  status: z.string(),
  netWorkId: z.string(),
  addressClientId: z.string(),
  addressOwnerId: z.string(),
  userAddress: z.string(),
  ownerId: z.string(),
  price: z.number()
})
export class balanceDto extends createZodDto(BalanceSchema) {}
