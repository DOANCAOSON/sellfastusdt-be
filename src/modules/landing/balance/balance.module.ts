import { Module } from '@nestjs/common'
import { BalancesController } from './balance.controller'
import { BalancesService } from './balance.service'

@Module({
  controllers: [BalancesController],
  providers: [BalancesService]
})
export class BalanceModule {}
