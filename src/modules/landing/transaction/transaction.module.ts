import { TokensService } from '@/modules/cms/token/token.service'
import { GetWayService } from '@/modules/gateway/gateway'
import { CMSAuthGuard } from '@/modules/guards/cms.guard'
import { HttpModule } from '@nestjs/axios' // Import HttpModule
import { Module } from '@nestjs/common'
import { ConfigService } from '@nestjs/config' // Import ConfigModule và ConfigService
import { APP_INTERCEPTOR } from '@nestjs/core'
import { BalancesService } from '../balance/balance.service'
import { NotifyService } from '../notify/notify.service'
import { TransactionController } from './transaction.controller'
import { TransactionService } from './transaction.service'

@Module({
  imports: [
    HttpModule.registerAsync({
      useFactory: () => ({
        timeout: 5000,
        maxRedirects: 5
      })
    })
  ],
  controllers: [TransactionController],
  providers: [
    TransactionService,
    { provide: APP_INTERCEPTOR, useClass: CMSAuthGuard },
    ConfigService,
    GetWayService,
    NotifyService,
    BalancesService,
    TokensService
  ]
})
export class TransactionModule {}
