import { IListQuery } from '@/modules/base/dto'
import { Public } from '@/modules/guards/public.guard'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Get, Param, Patch, Post, Req, UsePipes } from '@nestjs/common'
import { SellTransactionDto, TransactionDto, TransactionPriceDto } from './transaction.dto'
import { TransactionService } from './transaction.service'

@UsePipes(CacheInterceptor)
@Controller('landing/transaction')
export class TransactionController {
  constructor(private readonly transactionService: TransactionService) {}

  @Public()
  @Get()
  FindMany(@Req() req: IListQuery) {
    return this.transactionService.find(req)
  }

  @Public()
  @Post('price-pain-amount')
  getPriceAmount(@Body() body: TransactionPriceDto) {
    return this.transactionService.calculatePrice(body)
  }

  @Post('buy')
  PostBuy(@Body() body: TransactionDto, @Req() req: Request) {
    return this.transactionService.createBuyTransaction(body, req['ip'])
  }

  @Post('sell')
  PostSell(@Body() body: SellTransactionDto, @Req() req: Request) {
    return this.transactionService.createSellTransaction(body, req['ip'])
  }

  @Patch(':id')
  PatchTransaction(@Param('id') id: string, @Body() body: TransactionDto) {
    return this.transactionService.update(id, { ...body, id })
  }

  @Public()
  @Get(':id')
  FindOne(@Param('id') id: string) {
    return this.transactionService.findOne(id)
  }

  @Public()
  @Get('code/:code')
  FindOneByCode(@Param('code') code: string) {
    return this.transactionService.findOneByCode(code)
  }
}
