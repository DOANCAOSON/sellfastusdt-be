import { BaseDto } from '@/modules/base/dto'
import { BaseService } from '@/modules/base/service'
import { toId } from '@/modules/base/util'
import { GetWayService } from '@/modules/gateway/gateway'
import { HttpService } from '@nestjs/axios'
import { Inject, Injectable, NotFoundException, forwardRef } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { JwtService } from '@nestjs/jwt'
import * as dayjs from 'dayjs'
import * as duration from 'dayjs/plugin/duration'
import { SellTransactionDto, TransactionDto, TransactionPriceDto } from './transaction.dto'

dayjs.extend(duration)

@Injectable()
export class TransactionService extends BaseService {
  private readonly EXPIRATION_DURATION_SECONDS = 30

  constructor(
    private readonly configService: ConfigService,
    private jwtService: JwtService,
    private readonly httpService: HttpService,
    @Inject(forwardRef(() => GetWayService))
    private readonly GetWayService: GetWayService
  ) {
    super()
  }

  async calculatePrice(homeDto: TransactionPriceDto) {
    const { amount, price, transactionType, binance, walletId } = homeDto
    const token = binance ? await this._prisma.token.findFirst({ where: { binance } }) : null

    const Token = await this.GetWayService.getPriceCoin(binance)
    if (!Token) return this.withBadRequest(['Token is not found'])
    const wallet = await this._prisma.wallet.findFirst({ where: { id: walletId } })
    if (!wallet) return this.withBadRequest('Wallet is not found!')

    const { priceBuy, priceSell } = Token
    let result = 0

    const tokenName = transactionType === 'BUY' ? 'USDT' : token?.tokenName

    if (!!amount && !!priceBuy && !!priceSell && amount > 0) {
      result = transactionType === 'BUY' ? amount / priceBuy : amount * priceSell
      if (wallet.amount < amount) return { result, messageError: `Your ${tokenName} wallet balance is insufficient.` }
    } else if (!!price && !!priceBuy && !!priceSell && price > 0) {
      result = transactionType === 'BUY' ? price * priceBuy : price / priceSell
      if (wallet.amount < result) return { result, messageError: `Your ${tokenName} wallet balance is insufficient.` }
    }

    if (result > 0) return { result }
  }

  async find({ query }: any) {
    // TODO handle query by currency
    const { page = 1, pageSize = 10, userId } = query
    const { skip, take } = this.withLimitOffset(query)

    const [data, totalRecords] = await Promise.all([
      this._prisma.transaction.findMany({
        select: {
          id: true,
          code: true,
          createdAt: true,
          type: true,
          ethereumAmount: true,
          status: true,
          expired: true,
          ip: true,
          transactionToken: true,
          User: {
            select: {
              email: true,
              id: true
            }
          },
          OwnerUsers: {
            select: {
              email: true,
              id: true
            }
          },
          coinAmount: true,
          binance: true
        },
        where: {
          userId
        },
        skip,
        take,
        orderBy: {
          createdAt: 'desc'
        }
      }),
      this._prisma.transaction.count({
        where: query
      })
    ])

    const dataWithToken = await Promise.all(
      data.map(async (d) => {
        const token = await this._prisma.token.findFirst({ where: { binance: d.binance ?? '' } })
        d['token'] = token
        return d
      })
    )

    return { data: dataWithToken, pagination: { page, pageSize, totalRecords } }
  }

  async createBuyTransaction(transactionData: TransactionDto, ip: string) {
    const { ethereumAmount, coinAmount, userId, ownerUsersId, binance, ...data } = transactionData
    const code = this.generateCode()

    const transactionToken = await this.signTransactionToken(code)

    const now = new Date()
    const token = await this._prisma.token.findFirst({
      select: { id: true, images: true },
      where: { isActive: true }
    })

    const walletActive = await this._prisma.wallet.findFirst({
      where: { userId, binance: 'tusdusdt' },
      select: {
        amount: true
      }
    })
    if (ethereumAmount < 0 || coinAmount < 0) return await this.withBadRequest('Transaction amount is invalid')
    if ((walletActive && walletActive?.amount <= 0) || (walletActive && walletActive?.amount < coinAmount)) {
      return await this.withBadRequest('Transaction is invalid')
    }
    const transactionID = toId()
    const [transaction, notification] = await Promise.all([
      this._prisma.transaction.create({
        data: {
          ...data,
          id: transactionID,
          code,
          createdAt: now,
          binance,
          ip: transactionData.ip || ip,
          expired: dayjs.duration(this.EXPIRATION_DURATION_SECONDS, 'minutes').asSeconds(),
          status: 'NEW',
          ethereumAmount,
          coinAmount,
          transactionToken,
          userId,
          ownerUsersId
        },
        select: { createdAt: true, code: true, id: true }
      }),
      this._prisma.notification.create({
        data: {
          id: toId(),
          userId,
          url: `https://sellfastusdt.com/transaction/${data.type.toLowerCase()}?transactionID=${transactionID}`,
          content: `You have successfully created a ${data.type.toLowerCase()} transaction through the bank with the code ${code}`,
          image: token?.images,
          isRead: false,
          createdAt: now,
          text: 'Transaction successfully created!',
          updatedAt: now
        }
      })
    ])
    return { transaction, notification }
  }

  async createSellTransaction(transactionData: SellTransactionDto, ip: string) {
    const { ethereumAmount, userId, ownerUsersId, coinAmount, type, binance, ...data } = transactionData
    const code = this.generateCode()
    const transactionToken = await this.signTransactionToken(code)
    if (ethereumAmount < 0 || coinAmount < 0) return await this.withBadRequest('Transaction amount is invalid')
    const now = new Date()
    const token = await this._prisma.token.findFirst({
      select: { id: true, images: true },
      where: { isActive: true }
    })

    const transactionID = toId()
    const [transaction, notification] = await Promise.all([
      this._prisma.transaction.create({
        data: {
          id: transactionID,
          code,
          createdAt: now,
          ip: transactionData.ip || ip,
          expired: dayjs.duration(this.EXPIRATION_DURATION_SECONDS, 'minutes').asSeconds(),
          status: 'NEW',
          ethereumAmount,
          transactionToken,
          coinAmount,
          binance,
          type,
          User: {
            connect: { id: userId }
          } as any
        },
        select: { createdAt: true, code: true, id: true }
      }),
      this._prisma.notification.create({
        data: {
          id: toId(),
          userId,
          url: `https://sellfastusdt.com/transaction/${type.toLowerCase()}?transactionID=${transactionID}`,
          content: `You have successfully created a ${type.toLowerCase()} transaction through the bank with the code ${code}`,
          image: token?.images,
          isRead: false,
          createdAt: now,
          text: 'Transaction successfully created!',
          updatedAt: now
        }
      })
    ])
    return { transaction, notification }
  }

  async findOne(id: string) {
    const transaction = await this._prisma.transaction.findFirst({
      select: {
        transactionToken: true,
        id: true,
        type: true,
        binance: true,
        code: true,
        ethereumAmount: true
      },
      where: { id }
    })

    if (!transaction?.transactionToken) {
      return this.withBadRequest(['transaction: Transaction invalid'])
    }

    const detail = await this.verifyTransactionToken(transaction.transactionToken, transaction.id)
    const decodedToken = await this.jwtService
      .verifyAsync(transaction.transactionToken, {
        secret: this.configService.get('SECRET_TOKEN')
      })
      .catch(() => {
        detail && (detail['status'] = 'CANCELLED')
      })

    if (!detail) {
      return this.withBadRequest(['transaction: Transaction invalid'])
    }

    const token = await this._prisma.token.findFirst({
      where: { binance: transaction?.binance as any }
    })

    if (!detail) throw new NotFoundException()
    const currentTime = Math.floor(Date.now() / 1000)
    detail['createdBy'] = detail.User?.email

    detail['token'] = { img: token?.images, name: token?.tokenName, binance: token?.binance }

    detail['methodPayment'] = detail['binance']
    detail['ex'] = this.generateStatus(detail.status) as any
    const time = decodedToken?.exp - currentTime
    const expired = time > 0 && detail['status'] !== 'CANCELLED' && time
    detail['expired'] = expired as any
    return detail
  }

  async getQrCode(data: any) {
    try {
      const url = 'https://api.vietqr.io/v2/generate'
      const response = await this.httpService.axiosRef.post(url, data, {
        headers: {
          'x-client-id': 'fa08658c-56f1-4495-b712-a2a90074c874',
          'x-api-key': 'b27e4a30-6569-46c4-b639-ddff0f5571f3',
          'Content-Type': 'application/json'
        }
      })
      return response.data
    } catch (error) {
      console.error(`Error making request: ${error}`)
      return new this.withBadRequest('[qrCode: invalid]')
    }
  }

  async findOneByCode(code: string) {
    const detail = await this._prisma.transaction.findFirst({
      select: {
        code: true,
        id: true,
        type: true
      },
      where: { code }
    })
    if (!detail) {
      const balance = await this._prisma.balance.findFirst({
        select: {
          code: true,
          id: true,
          type: true
        },
        where: { code }
      })

      if (!balance) return this.withBadRequest(['transaction: Transaction invalid'])
      return balance
    }
    return detail
  }

  async update(id: string, input: TransactionDto & BaseDto) {
    const transaction = await this.getTransactionDetails(id)

    if (!transaction?.transactionToken) {
      return this.handleInvalidTransaction(id, input)
    } else {
      return this.updateTransaction(id, input, transaction)
    }
  }

  private async signTransactionToken(code: string) {
    return this.jwtService.signAsync(
      { code },
      { secret: this.configService.get('SECRET_TOKEN'), expiresIn: `${this.EXPIRATION_DURATION_SECONDS}m` }
    )
  }

  private async verifyTransactionToken(token: string, id: string) {
    return await this.jwtService
      .verifyAsync(token, { secret: this.configService.get('SECRET_TOKEN') })
      .then(async () => {
        return await this._prisma.transaction.findFirst({
          select: {
            id: true,
            code: true,
            createdAt: true,
            type: true,
            ethereumAmount: true,
            status: true,
            expired: true,
            ip: true,
            coinAmount: true,
            transactionToken: true,
            binance: true,
            User: {
              select: {
                email: true,
                id: true
              }
            },
            OwnerUsers: {
              select: {
                email: true,
                id: true
              }
            },
            histories: true
          },
          where: { id }
        })
      })
      .catch(async () => {
        const detail = await this._prisma.transaction.update({
          select: {
            id: true,
            code: true,
            createdAt: true,
            type: true,
            coinAmount: true,
            ethereumAmount: true,
            status: true,
            expired: true,
            ip: true,
            transactionToken: true,
            histories: true,
            User: {
              select: {
                email: true,
                id: true
              }
            },
            OwnerUsers: {
              select: {
                email: true,
                id: true
              }
            }
          },
          where: { id },
          data: {
            status: 'CANCELLED'
          }
        })
        detail['status'] = 'CANCELLED'
        return detail
      })
  }

  private generateStatus(status: string) {
    switch (status) {
      case 'NEW':
        return { type: status, message: 'Waiting to receive money' }
      case 'PAID':
        return { type: status, message: 'Received money' }
      case 'CONFIRMED':
        return { type: status, message: 'Accomplished' }
      case 'CANCELLED':
        return { type: status, message: 'Cancelled' }
    }
  }

  private generateCode() {
    return 'CB-' + Math.floor(1000000000 + Math.random() * 9000000000)
  }

  private async getTransactionDetails(id: string) {
    if (id)
      return this._prisma.transaction.findFirst({
        where: { id },
        select: {
          status: true,
          type: true,
          userId: true,
          transactionToken: true
        }
      })
    return null
  }

  private async handleInvalidTransaction(id: string, input: TransactionDto & BaseDto) {
    const transaction = await this.getTransactionDetails(id)
    if (!transaction) return NotFoundException

    return this._prisma.transaction.update({
      where: { id },
      data: {
        status: 'CANCELLED',
        updatedAt: new Date(),
        histories: {
          create: {
            id: toId(),
            createdAt: new Date(),
            userId: transaction.userId ?? '',
            action: [{ content: JSON.stringify(input), createdAt: new Date() }],
            type: transaction.type
          }
        }
      },
      select: { updatedAt: true, id: true, userId: true }
    })
  }

  private async updateTransaction(id: string, input: TransactionDto & BaseDto, transaction: any) {
    return this._prisma.transaction.update({
      where: { id },
      data: {
        ...input,
        ip: input.ip ?? '',
        updatedAt: new Date(),
        histories: {
          create: {
            id: toId(),
            createdAt: input['updatedAt'],
            userId: transaction.buyerId ?? '',
            action: [{ content: JSON.stringify(input), createdAt: input['updatedAt'] }],
            type: transaction.type
          }
        }
      },
      select: { updatedAt: true, id: true, userId: true }
    })
  }
}
