import { Module } from '@nestjs/common'
import { APP_GUARD } from '@nestjs/core'
import { JwtModule } from '@nestjs/jwt'
import { CMSAuthGuard } from '../guards/cms.guard'
import { ArticleModule } from './article/article.module'
import { AuthModule } from './auth/auth.module'
import { BalanceModule } from './balance/balance.module'
import { HomeModule } from './home/home.module'
import { TokenModule } from './token/token.module'
import { TransactionModule } from './transaction/transaction.module'
import { UsersModule } from './user/user.module'
import { NotifyModule } from './notify/notify.module'

@Module({
  imports: [
    AuthModule,
    HomeModule,
    BalanceModule,
    ArticleModule,
    NotifyModule,
    UsersModule,
    JwtModule.register({
      global: true,
      secret: process.env.SECRET_TOKEN || 'SECRET_TOKEN',
      signOptions: { expiresIn: '30m' }
    }),
    TransactionModule,
    TokenModule
  ],
  providers: [{ provide: APP_GUARD, useClass: CMSAuthGuard }]
})
export class LandingModule {}
