import { Public } from '@/modules/guards/public.guard'
import { Body, Controller, Post } from '@nestjs/common'
import { ConfirmDto, ResendOTPDto, SignInDto } from './auth.dto'
import { AuthService } from './auth.service'

@Controller('landing/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Public()
  @Post('confirm')
  async confirm(@Body() body: ConfirmDto) {
    return this.authService.confirm(body)
  }

  @Public()
  @Post('sign-in')
  async signin(@Body() body: SignInDto) {
    return this.authService.signin(body)
  }

  @Public()
  @Post('resend-otp')
  async resend(@Body() body: ResendOTPDto) {
    return this.authService.resendOTP(body)
  }
}
