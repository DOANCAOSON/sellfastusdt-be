import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const SignInSchema = z.object({
  email: z.string().max(30)
})
export class SignInDto extends createZodDto(SignInSchema) {}

const ConfirmSchema = z.object({ email: z.string(), otp: z.string() })
export class ConfirmDto extends createZodDto(ConfirmSchema) {}

const ResendOTPSchema = z.object({ email: z.string() })
export class ResendOTPDto extends createZodDto(ResendOTPSchema) {}
