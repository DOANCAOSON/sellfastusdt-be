import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { JwtService } from '@nestjs/jwt'
import * as nodemailer from 'nodemailer'
import { BaseService } from '../../base/service'
import { toId } from '../../base/util'
import { ConfirmDto, ResendOTPDto, SignInDto } from './auth.dto'

@Injectable()
export class AuthService extends BaseService {
  constructor(private readonly configService: ConfigService, private jwtService: JwtService) {
    super()
  }

  async signin(input: SignInDto) {
    const { email } = input
    let user

    user = await this._prisma.user.findUnique({
      select: { id: true, confirmationToken: true },
      where: { email }
    })

    const wallets = await this._prisma.wallet.findMany({
      where: { userId: user?.id }
    })

    return this._prisma.$transaction(
      async (tx) => {
        const createdAt = new Date()

        if (!!user) {
          await tx.user.update({
            where: { id: user.id },
            data: {
              email,
              updatedAt: createdAt
            },
            select: { createdAt: true }
          })
        } else {
          user = await tx.user.create({
            data: {
              id: toId(),
              email,
              createdAt
            },
            select: { createdAt: true, id: true }
          })
        }
        await this._prisma.oTP.deleteMany({ where: { userId: user.id } })
        const otp = await this.createOTPAsync({ userId: user.id, email })

        setTimeout(() => {
          // send confirmation email
          this.sendConfirmation(email, otp)
        }, 0)
        return { createdAt, email, ...user, wallets }
      },
      { isolationLevel: 'ReadCommitted', maxWait: 10000, timeout: 10000 }
    )
  }

  async confirm(input: ConfirmDto) {
    const { email, otp } = input

    if (!email || !otp) {
      return this.withBadRequest('Empty otp detail are not allowed')
    } else {
      const oTP = await this._prisma.oTP.findFirst({
        where: { otp },
        select: { otp: true, expiresAt: true, id: true }
      })

      if (!oTP) return this.withBadRequest('Account record doesn`t exits')
      if (oTP.expiresAt < Date.now()) {
        await this._prisma.oTP.delete({
          where: { id: oTP.id }
        })
        return this.withBadRequest('Code has expired. Please request again.')
      }
      const user = await this._prisma.user.findFirst({
        where: { email },
        select: { email: true, confirmationToken: true, id: true }
      })
      if (!user) {
        return this.withBadRequest(['token: Token invalid'])
      }
      if (user.confirmationToken === oTP.otp) {
        return this.withBadRequest(['token: Token invalid'])
      }
      await this._prisma.user.update({
        where: { id: user.id },
        data: {
          updatedAt: new Date(),
          confirmationToken: oTP.otp
        },
        select: { createdAt: true, id: true }
      })

      const tokens = await this._prisma.token.findMany({})

      const wallets = await this.findOrCreateWallets(tokens, user)

      return {
        ...user,
        token: await this.jwtService.signAsync(
          { id: user.id, otp },
          {
            secret: this.configService.get('SECRET_TOKEN'),
            expiresIn: `30m`
          }
        ),
        wallets
      }
    }
  }

  async resendOTP(input: ResendOTPDto) {
    let { email } = input
    if (!email) return this.withBadRequest('Empty user details are not allowed')
    const user = await this._prisma.user.findFirst({ where: { email } })
    if (!user) return this.withBadRequest('Empty user details are not allowed')
    await this._prisma.oTP.deleteMany({ where: { userId: user.id } })
    const otp = await this.createOTPAsync({ userId: user.id, email })

    setTimeout(() => {
      // send confirmation email
      this.sendConfirmation(email, otp)
    }, 0)
    return { otp }
  }

  private async createOTPAsync({ userId, email }) {
    const otp = `${Math.floor(1000 + Math.random() * 9000)}`
    await this._prisma.oTP.create({
      data: {
        id: toId(),
        createdAt: new Date(),
        expiresAt: Date.now() + 1800000,
        otp: otp,
        userId
      }
    })

    return otp
  }

  private async findOrCreateWallets(tokens, user) {
    const walletPromises = tokens.map((token) => {
      return this._prisma.wallet.findMany({
        where: { userId: user?.id, tokenId: token.id },
        select: {
          Token: true,
          id: true,
          userId: true,
          tokenId: true,
          amount: true,
          binance: true,
          address: true,
          isActive: true
        }
      })
    })

    const walletsResults = await Promise.all(walletPromises)

    const wallets = await Promise.all(
      walletsResults.map(async (wallets, index) => {
        if (wallets.length === 0) {
          const token = tokens[index]
          return await this._prisma.wallet.create({
            data: {
              userId: user.id,
              tokenId: token.id,
              binance: token.binance,
              id: toId(),
              createdAt: new Date(),
              updatedAt: new Date()
            },
            select: {
              amount: true,
              binance: true,
              createdAt: true,
              isActive: true,
              Token: true,
              User: true,
              userId: true,
              updatedAt: true
            }
          })
        } else {
          return wallets[0]
        }
      })
    )

    return wallets
  }

  private async sendConfirmation(email: string, otp: string) {
    const EMAIL = 'sellfastusdt@gmail.com'

    const transport = nodemailer.createTransport({
      service: 'gmail',
      secure: false,
      auth: {
        user: EMAIL,
        pass: 'efjb sdcu xicd jeks'
      },
      tls: {
        rejectUnauthorized: false
      }
    })

    transport.sendMail(
      {
        to: email,
        from: EMAIL,
        text: 'SellfastUSDT',
        subject: 'Verify action Account',
        html: `<!DOCTYPE html>
<html>
<head>
    <title>Welcome!</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        body, table, td, a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }
        table, td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }
        img {
            -ms-interpolation-mode: bicubic;
        }
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }
        table {
            border-collapse: collapse !important;
        }
        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
            background-color: #e1e1e1;
        }
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }
    </style>
</head>
<body>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td bgcolor="#18a772" align="center">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td align="center" valign="top" style="padding: 40px 10px;"> </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#18a772" align="center" style="padding: 0 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; line-height: 48px;">
                            <h1 style="font-size: 48px; font-weight: 400; margin: 2;">Welcome you!</h1> 
                            <img src="https://sellfastusdt.com/assets/images/logo.png" width="125" height="120" style="display: block; border: 0;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#e1e1e1" align="center" style="padding: 0 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0;">Help us protect your ${email}!</p>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <p style="margin: 0;">Before you sign in, we need to verify your identity. Enter the following code on the sign-in page: ${otp}</p>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" align="center" style="padding: 20px 30px 40px;">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="center" style="border-radius: 3px; width: 207px; height: 53px; background-color: #f0f0f0; line-height: 53px; font-weight: 700; font-size: 1.5em; color: #303030;" bgcolor="#18a772">${otp}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 0 30px 20px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 25px;">
                            The link above will expire within 30 minutes and can only be used once. Thank you for using sellfastusdt.com
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
`
      },
      function (error, info) {
        if (error) {
          console.log(error)
        } else {
          console.log('Verfication email is sent to your gmail account')
        }
      }
    )
  }

  // private async sendConfirmation(email: string, otp: string) {
  //   const EMAIL = 'admin@sellfastusdt.com'

  //   const TOKEN = 'b8ad55c9f3a565e0ab5592fb442051de'

  //   const client = new MailtrapClient({ token: TOKEN })

  //   const sender = { name: 'Mailtrap Test', email: EMAIL }

  //   try {
  //     const res = await client.send({
  //       from: sender,
  //       to: [{ email }],
  //       subject: 'Verify action Account',
  //       text: FORM_REGISTER.HtmlPart.replace('{email}', email).replace('{otp}', otp)
  //     })

  //     console.log(res)
  //   } catch (error) {
  //     console.log(error)
  //     return this.withBadRequest('Login failed')
  //   }
  // }
}
