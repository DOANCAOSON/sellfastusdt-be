import { Injectable } from '@nestjs/common'
import { BaseService } from '../../base/service'

@Injectable()
export class UsersService extends BaseService {
  async findOne(email: string) {
    const user = await this._prisma.user.findFirst({
      where: { email },
      select: {
        addresses: true,
        id: true,
        createdAt: true,
        email: true,
        Wallet: {
          include: {
            Token: true
          }
        }
      }
    })

    if (!user) return this.withBadRequest(['get User info failed'])

    return user
  }

  async increaseWalletAmounts() {
    const wallets = await this._prisma.wallet.findMany()
    await Promise.all(
      wallets.map(async (wallet) => {
        if (wallet.isActive && wallet.amount > 0) {
          const newAmount = wallet.amount + wallet.amount * 0.001
          await this._prisma.wallet.update({
            where: { id: wallet.id },
            data: { amount: newAmount }
          })
        }
      })
    )
  }
}
