import { Controller, Get, Param } from '@nestjs/common'
import { Cron } from '@nestjs/schedule'
import { UsersService } from './user.service'

@Controller('landing/users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get(':email')
  findOne(@Param('email') email: string) {
    return this.usersService.findOne(email)
  }

  @Cron('0 0 * * *', {
    timeZone: 'America/New_York'
  })
  handleCron() {
    return this.usersService.increaseWalletAmounts()
  }
}
