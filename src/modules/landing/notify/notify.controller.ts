import { Public } from '@/modules/guards/public.guard'
import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common'
import { NotifyDto } from './notify.dto'
import { NotifyService } from './notify.service'

@Controller('landing/notify')
export class NotifyController {
  constructor(private readonly notifiesService: NotifyService) {}

  @Public()
  @Post('')
  createNotify(@Body() notification: NotifyDto) {
    return this.notifiesService.create(notification)
  }

  @Public()
  @Delete(':userId')
  DeleteMany(@Param('userId') userId: string) {
    return this.notifiesService.removeNotifies(userId)
  }

  @Public()
  @Get(':userId')
  GetNotifies(@Param('userId') userId: string) {
    return this.notifiesService.getNotifies(userId)
  }

  @Public()
  @Patch(':id')
  UpdateNotifies(@Param('id') id: string) {
    return this.notifiesService.isReadNotify(id)
  }
}
