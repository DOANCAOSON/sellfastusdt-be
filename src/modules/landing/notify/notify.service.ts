import { toId } from '@/modules/base/util'
import { Injectable } from '@nestjs/common'
import { BaseService } from '../../base/service'
import { NotifyDto } from './notify.dto'

@Injectable()
export class NotifyService extends BaseService {
  async create(notification: NotifyDto) {
    const { ...data } = notification

    return this._prisma.$transaction(
      async (tx) => {
        return tx.notification.create({
          data: {
            ...data,
            createdAt: new Date(),
            updatedAt: new Date(),
            isRead: false,
            id: toId()
          }
        })
      },
      { isolationLevel: 'ReadCommitted', maxWait: 10000, timeout: 10000 }
    )
  }

  async removeNotifies(userId: string) {
    const output = await this._prisma.$transaction(
      async (tx) => {
        await Promise.all([tx.notification.deleteMany({ where: { userId } })])
        return { userId }
      },
      { isolationLevel: 'ReadCommitted', maxWait: 10000, timeout: 10000 }
    )

    return output
  }

  async getNotifies(userId: string) {
    const [dataSource, total] = await Promise.all([
      this._prisma.notification.findMany({
        where: { userId },
        orderBy: { createdAt: 'desc' }
      }),
      this._prisma.notification.count({ where: { userId } })
    ])

    return { dataSource, total }
  }

  async isReadNotify(id: string) {
    return this._prisma.$transaction(
      async (tx) => {
        return tx.notification.update({
          where: { id },
          data: { isRead: true }
        })
      },
      { isolationLevel: 'ReadCommitted', maxWait: 10000, timeout: 10000 }
    )
  }
}
