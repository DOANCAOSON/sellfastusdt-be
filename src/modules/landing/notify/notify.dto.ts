import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const NotifySchema = z.object({
  url: z.string(),
  text: z.string(),
  content: z.string(),
  image: z.string(),
  userId: z.string()
})
export class NotifyDto extends createZodDto(NotifySchema) {}
