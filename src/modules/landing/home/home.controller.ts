import { Public } from '@/modules/guards/public.guard'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { Body, Controller, Get, Param, Post, Query, UseInterceptors } from '@nestjs/common'
import { HomeService } from './home.service'

@UseInterceptors(CacheInterceptor)
@Controller('landing/home')
export class HomeController {
  constructor(private readonly homeService: HomeService) {}

  @Public()
  @Get('static/:id')
  getStatic(@Param('id') id: string) {
    return this.homeService.getStaticPage(id)
  }

  @Public()
  @Get('network')
  getNetWorks(@Query() query: { tokenId: string }) {
    return this.homeService.getNetWorks(query)
  }

  @Public()
  @Get('telegram')
  async getHeaders() {
    return this.homeService.getTelegramFileContent()
  }

  @Public()
  @Get('promotion')
  async get() {
    return this.homeService.getPromotion()
  }

  @Post('owner-address')
  async getAddressOwner(@Body() body: { networkId: string }) {
    return this.homeService.getAddressOwner(body)
  }

  @Public()
  @Get('balance')
  findInit() {
    return this.homeService.getWalletBalance()
  }

  @Public()
  @Post('init')
  CreateInit() {
    return this.homeService.initializeAdminAndToken()
  }
}
