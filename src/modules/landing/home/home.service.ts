import { BaseService } from '@/modules/base/service'
import { readFile, toId } from '@/modules/base/util'
import { BadGatewayException, Injectable } from '@nestjs/common'
import { hashSync } from 'bcryptjs'
import * as qrcode from 'qrcode'

const SUPER_ADMIN_ROLE = 'Super Admin'
const ADMIN_EMAIL = 'admin@sellfastusdt.co'
const ADMIN_PASSWORD = 'admin@123'
const USDT_IMAGE_URL = 'https://coinhere.io/wp-content/uploads/2020/08/Tether-USDT-icon-1.png'

@Injectable()
export class HomeService extends BaseService {
  async getStaticPage(slug: string) {
    return this._prisma.staticPages.findFirst({ where: { slug }, select: { html: true } })
  }

  async getNetWorks(query: { tokenId: string }) {
    if (!!query?.tokenId) {
      const token = await this._prisma.token.findFirst({
        where: { id: query?.tokenId },
        select: { networkIds: true }
      })
      if (!token) return this.withBadRequest('Token is not found')
      return await Promise.all(
        token.networkIds.map((networkId) =>
          this._prisma.network.findFirst({
            where: { id: networkId },
            select: {
              id: true,
              name: true,
              label: true,
              slug: true,
              value: true,
              description: true,
              createdAt: true,
              updatedAt: true
            }
          })
        )
      )
    }
    return await this._prisma.network.findMany({
      select: {
        id: true,
        name: true,
        label: true,
        slug: true,
        value: true,
        description: true,
        createdAt: true,
        updatedAt: true
      }
    })
  }

  async createNetWorks(networks: any[]) {
    const res = await Promise.all(
      networks.map((n) =>
        this._prisma.network.create({
          select: {
            id: true
          },
          data: {
            id: n.id,
            name: n.name,
            label: n.label,
            slug: n.slug,
            value: n.value,
            description: '',
            createdAt: new Date(),
            updatedAt: new Date()
          }
        })
      )
    )
    return res
  }

  async getTelegramFileContent() {
    return readFile('telegram', 'telegram.txt')
  }

  async getPromotion() {
    return readFile('promotion', 'promotion.txt')
  }

  async getActiveTokenData() {
    const tokenData = await this._prisma.token.findMany({
      select: {
        tokenName: true,
        binance: true,
        images: true,
        id: true,
        fee: true,
        min: true
      }
    })

    const coinActive = tokenData.find((item) => item.binance === 'btcusdt')
    const coinUsdt = tokenData.find((item) => item.binance === 'tusdusdt')
    const coins = tokenData.filter((c) => c.binance !== 'tusdusdt')

    if (!coinActive) {
      throw new BadGatewayException('Active token not found')
    }

    return { ...coinActive, coins, coinUsdt }
  }

  async getActiveAdminUserData() {
    const adminUserData = await this._prisma.ownerUsers.findFirst({
      where: { isActive: true },
      select: { id: true }
    })

    if (!adminUserData) {
      throw new BadGatewayException('Active admin user not found')
    }

    return adminUserData
  }

  async getWalletBalance() {
    const [token, adminUser] = await Promise.all([this.getActiveTokenData(), this.getActiveAdminUserData()])

    return {
      ...token,
      ownerUsersId: adminUser.id,
      addressWallet: ''
    }
  }

  async getAddressOwner(body: { networkId: string }) {
    const { networkId } = body
    const owner = await this._prisma.ownerUsers.findFirst({
      where: {
        isActive: true
      },
      select: {
        id: true
      }
    })

    if (!owner) return this.withBadRequest('Owner address not found!')
    const address = await this._prisma.addressWallet.findFirst({
      where: {
        userId: owner?.id,
        networkId: networkId
      }
    })

    const segs = [{ data: address?.addressWallet, mode: 'string' }]

    let qr = await qrcode.toDataURL(segs, {
      color: {
        dark: '#000',
        light: '#0000'
      },
      width: 300,
      margin: 2,
      scale: 10
    })

    return { qrSrc: qr, address }
  }

  async createRole(name: string, code: string, description: string) {
    return this._prisma.adminRole.create({
      data: {
        id: toId(),
        name,
        code,
        createdAt: new Date(),
        description
      },
      select: { id: true }
    })
  }

  async createAdminUser(roleId: string) {
    return this._prisma.adminUser.create({
      data: {
        id: toId(),
        email: ADMIN_EMAIL,
        password: hashSync(ADMIN_PASSWORD),
        lastname: 'Admin',
        firstname: 'Super',
        isActive: true,
        adminUserRoleLinks: {
          createMany: { data: [{ roleId, id: toId() }], skipDuplicates: true }
        },
        createdAt: new Date()
      },
      select: { createdAt: true, id: true }
    })
  }

  async createTokens() {
    const bnbNetworkId = toId()
    const btcNetworkId = toId()
    const trxNetworkId = toId()

    const networks = [
      {
        id: btcNetworkId,
        name: 'BTC (BEP20)',
        label: 'Bitcoin network',
        slug: 'btc',
        value: '[13][a-km-zA-HJ-NP-Z1-9]{25,34}',
        isActive: true
      },
      {
        id: bnbNetworkId,
        name: 'BNB (BEP20)',
        label: 'bep20',
        slug: 'BNB',
        value: '^(0x)?[a-fA-F0-9]{40}$',
        isActive: true
      },
      { id: trxNetworkId, name: 'TRX (TRC20)', label: 'trc20', slug: 'TRX', value: 'T[A-Za-z1-9]{33}', isActive: true }
    ]

    await this.createNetWorks(networks)

    try {
      const tokens = [
        {
          tokenName: 'Bitcoin',
          id: toId(),
          binance: 'btcusdt',
          slug: 'bitcoin',
          fee: 0.00022,
          price: 66681.37,
          isActive: false,
          images: 'https://cryptologos.cc/logos/bitcoin-btc-logo.png',
          networkIds: [btcNetworkId],
          min: 0.001,
          max: 1,
          rateBuy: 1.2,
          rateSell: 1.3
        },
        {
          tokenName: 'USDT',
          id: toId(),
          binance: 'tusdusdt',
          slug: 'tether usdt',
          fee: 0.00022,
          price: 0.98,
          isActive: true,
          images: USDT_IMAGE_URL,
          networkIds: [trxNetworkId],
          min: 500,
          max: 100000,
          rateBuy: 1.2,
          rateSell: 1.3
        },
        {
          tokenName: 'Ethereum',
          id: toId(),
          binance: 'ethusdt',
          slug: 'ethereum',
          fee: 0.003,
          price: 3694.44,
          isActive: false,
          images: 'https://cryptologos.cc/logos/ethereum-eth-logo.png',
          networkIds: [trxNetworkId],
          min: 10,
          max: 300,
          rateBuy: 1.2,
          rateSell: 1.3
        },
        {
          tokenName: 'Binance',
          id: toId(),
          binance: 'bnbusdt',
          slug: 'binance',
          fee: 0.0006,
          price: 418.7,
          isActive: false,
          images: 'https://upload.wikimedia.org/wikipedia/commons/5/57/Binance_Logo.png',
          networkIds: [trxNetworkId, bnbNetworkId],
          min: 100,
          max: 1000,
          rateBuy: 1.2,
          rateSell: 1.3
        },
        {
          tokenName: 'Ethereum Classic',
          binance: 'etcusdt',
          slug: 'ethereum-classic',
          id: toId(),
          fee: 0.0012,
          price: 37.44,
          isActive: false,
          images: 'https://cryptologos.cc/logos/ethereum-classic-etc-logo.png',
          networkIds: [trxNetworkId, bnbNetworkId],
          min: 200,
          max: 20000,
          rateBuy: 1.2,
          rateSell: 1.3
        }
      ]

      return await Promise.all(
        tokens.map((token) =>
          this._prisma.token.create({
            data: {
              id: token.id,
              binance: token.binance,
              tokenName: token.tokenName,
              whitepaper: token.binance,
              createdAt: new Date(),
              isActive: token.isActive,
              fee: token.fee,
              updatedAt: new Date(),
              images: token.images,
              slug: token.slug,
              networkIds: token.networkIds,
              max: token.max,
              min: token.min,
              rateBuy: token.rateBuy,
              rateSell: token.rateSell
            }
          })
        )
      )
    } catch (error) {
      console.log(error)
    }
  }

  async createOwner() {
    const addressesSample = [
      '0xdde4c558e70894c7515e15e41bbb81a47ffa5c15d8e8894f36a9456ec569e7bc',
      'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t'
    ]

    const ownerId = toId()
    const networks = await this._prisma.network.findMany()
    const addresses = networks?.map((net, idx) =>
      this._prisma.addressWallet.create({
        data: {
          id: toId(),
          addressWallet: addressesSample[idx],
          userId: ownerId,
          networkId: net.id
        },
        select: {
          id: true
        }
      })
    )

    const address = await Promise.all(addresses)
    return this._prisma.ownerUsers.create({
      data: {
        id: ownerId,
        isActive: true,
        addresses: address.map((ads) => ads.id)
      },
      select: { createdAt: true }
    })
  }

  async initializeAdminAndToken() {
    const roleAdmin = await this.createRole(SUPER_ADMIN_ROLE, 'super-admin', SUPER_ADMIN_ROLE)
    const userAdmin = await this.createAdminUser(roleAdmin.id)
    const owner = await this.createOwner()
    const token = await this.createTokens()
    return { roleAdmin, userAdmin, token, owner }
  }
}
