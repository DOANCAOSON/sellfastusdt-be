import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const HomeSchema = z.object({
  amount: z.number(),
  transactionType: z.string(),
  model: z.string(),
  price: z.number(),
  binance: z.string(),
  walletId: z.string()
})
export class HomeDto extends createZodDto(HomeSchema) {}
