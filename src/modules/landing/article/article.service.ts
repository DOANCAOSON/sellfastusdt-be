import { IListQuery } from '@/modules/base/dto'
import { Injectable } from '@nestjs/common'
import { BaseService } from '../../base/service'

@Injectable()
export class ArticlesService extends BaseService {
  async findAll(query: IListQuery) {
    const { skip, take } = this.withLimitOffset({ page: query.page, pageSize: query.pageSize })
    const { page = 1, pageSize = 10 } = query
    const [data, totalRecords] = await Promise.all([
      this._prisma.article.findMany({
        select: {
          id: true,
          title: true,
          description: true,
          slug: true,
          createdAt: true,
          images: true,
          createdBy: { select: { id: true } }
        },
        where: { published: true },
        skip,
        take
      }),
      this._prisma.article.count({
        where: { published: true }
      })
    ])
    return { data, pagination: { page: page, pageSize: pageSize, totalRecords } }
  }

  async findOne(slug: string) {
    return await this._prisma.article.findFirst({
      where: { slug, published: true },
      select: {
        title: true,
        createdAt: true,
        slug: true,
        description: true,
        body: true,
        images: true,
        updatedAt: true
      }
    })
  }
}
