import { createZodDto } from '@anatine/zod-nestjs'
import { z } from 'zod'

const ArticleSchema = z.object({
  title: z.string().max(30),
  description: z.string().max(100).optional().nullable(),
  body: z.string().max(1000),
  published: z.boolean()
})
export class ArticleDto extends createZodDto(ArticleSchema) {}
