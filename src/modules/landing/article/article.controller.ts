import { IListQuery } from '@/modules/base/dto'
import { Public } from '@/modules/guards/public.guard'
import { Controller, Get, Param, Query } from '@nestjs/common'
import { ArticlesService } from './article.service'

@Controller('landing/articles')
export class ArticlesController {
  constructor(private readonly articlesService: ArticlesService) {}

  @Public()
  @Get('/find')
  findDrafts(@Query() query: IListQuery) {
    return this.articlesService.findAll(query)
  }

  @Public()
  @Get(':slug')
  findOne(@Param('slug') id: string) {
    return this.articlesService.findOne(id)
  }
}
