import { BaseDto } from './dto'

export class BaseController {
  withUpdated(body: any, req: Request) {
    const input = body as BaseDto
    const now = new Date()
    if (req.method === 'POST') {
      input.createdById = req['user'].id
      input.createdAt = now
    }
    input.updatedById = req['user'].id
    input.updatedAt = now
    return body
  }
}
