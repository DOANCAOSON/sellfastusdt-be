export interface BaseDto {
  id: string
  createdById?: string
  updatedById?: string
  createdAt?: Date
  updatedAt?: Date
}

export interface CurrentUser {
  id: string
  email: string
  roles: string[]
}

export interface IFilter {
  name: string
  type: string
}

export interface IListQuery extends Record<string, unknown> {
  page: number
  pageSize: number
  total?: number
}

export interface IAuthQuery extends Record<string, unknown> {
  query: { token: string }
}

export interface IListDto {
  dataSource: Array<Record<string, unknown>>
  pagination: IListQuery
}
