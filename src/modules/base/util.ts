import * as dayjs from 'dayjs'
import { existsSync, mkdirSync, readFileSync, unlinkSync, writeFileSync } from 'fs'
import { customAlphabet } from 'nanoid'
import { join } from 'path'

const ALPHA_NUMERIC = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

export const toId = () => customAlphabet(ALPHA_NUMERIC, 16)()

export const toSlug = (str: string) => {
  str = str.replace(/A|Á|À|Ã|Ạ|Â|Ấ|Ầ|Ẫ|Ậ|Ă|Ắ|Ằ|Ẵ|Ặ/g, 'A')
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
  str = str.replace(/E|É|È|Ẽ|Ẹ|Ê|Ế|Ề|Ễ|Ệ/, 'E')
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
  str = str.replace(/I|Í|Ì|Ĩ|Ị/g, 'I')
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
  str = str.replace(/O|Ó|Ò|Õ|Ọ|Ô|Ố|Ồ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ỡ|Ợ/g, 'O')
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
  str = str.replace(/U|Ú|Ù|Ũ|Ụ|Ư|Ứ|Ừ|Ữ|Ự/g, 'U')
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
  str = str.replace(/Y|Ý|Ỳ|Ỹ|Ỵ/g, 'Y')
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
  str = str.replace(/Đ/g, 'D')
  str = str.replace(/đ/g, 'd')
  // Some system encode vietnamese combining accent as individual utf-8 characters
  str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, '')
  str = str.replace(/\u02C6|\u0306|\u031B/g, '')
  return str
    .replace(/  +/g, '-')
    .replace(/ /g, '-')
    .replace(/[^A-Za-z0-9-]/g, '')
    .toLowerCase()
}

export const saveImageFile = async (
  uploads: Array<{ thumbUrl: string; name: string; type: string }>,
  prefix: string
) => {
  const baseDir = join(process.cwd(), 'static', 'images', prefix)
  if (!existsSync(baseDir)) {
    mkdirSync(baseDir, { recursive: true })
  }
  const output = await Promise.allSettled(
    uploads.map((u) => {
      const regex = new RegExp(`^data:${u.type};base64,`)
      const base64 = u.thumbUrl.replace(regex, '')
      writeFileSync(join(baseDir, u.name), base64, { flag: 'w', encoding: 'base64' })
      return join('static', 'images', prefix, u.name)
    })
  )
  return output.filter((o) => o.status === 'fulfilled').map((o) => o['value'])
}

export const removeImageFile = (images: string | null) => {
  if (images) {
    images.split(',').forEach((i) => {
      if (existsSync(join(process.cwd(), i))) unlinkSync(join(process.cwd(), i))
    })
  }
}

export const saveFile = (dir: string, filename: string, content: string) => {
  const baseDir = join(process.cwd(), 'static', dir)
  if (!existsSync(baseDir)) {
    mkdirSync(baseDir, { recursive: true })
  }
  writeFileSync(join(baseDir, filename), content, { flag: 'w', encoding: 'utf-8' })
}

export const readFile = (dir: string, filename: string) => {
  const file = join(process.cwd(), 'static', dir, filename)
  if (existsSync(file)) {
    return readFileSync(file, 'utf-8')
  }
  return ''
}

export const toCurrency = (val: number, currency = 'VND') => {
  return val?.toLocaleString('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 0
  })
}

export const strToDate = (str: string, startOrEnd?: 'start' | 'end') => {
  const d = dayjs(str)
  if (startOrEnd === 'start') {
    return d.startOf('day').toISOString()
  } else if (startOrEnd === 'end') {
    return d.endOf('day').toISOString()
  }

  return d.toDate()
}

export const dateToStr = (d: Date | string | null, format = 'YYYY-MM-DD') => {
  return dayjs(d).format(format)
}

export const addDateToStr = (
  d: Date | string | null,
  add: number,
  unit: dayjs.ManipulateType,
  format = 'YYYY-MM-DD'
) => {
  return dayjs(d).add(add, unit).format(format)
}

export const diff = (d1: string | Date | null, unit: dayjs.UnitType, d2?: Date | string) => {
  return dayjs(d1).diff(d2, unit)
}

export const isBefore = (d1: string, d2: string) => {
  return dayjs(d1).isBefore(d2)
}
