import { NestFactory } from '@nestjs/core'
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify'
import 'dotenv/config'
import { AppModule } from './app.module'

async function bootstrap() {
  const { exec } = await import('child_process')
  exec('yarn prisma migrate deploy', { env: process.env }, async (error, stdout, stderr) => {
    if (error) {
      console.trace(error)
      return
    }
    if (stderr) {
      console.log(`stderr: ${stderr}`)
      return
    }
    console.log(`stdout: ${stdout}`)
    const app = await NestFactory.create<NestFastifyApplication>(
      AppModule,
      new FastifyAdapter({ logger: true, caseSensitive: true, bodyLimit: 10048576 })
    )
    app.enableCors()
    await app.listen(33334, '0.0.0.0')
  })
}
bootstrap()
